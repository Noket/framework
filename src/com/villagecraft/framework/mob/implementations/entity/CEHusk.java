 package com.villagecraft.framework.mob.implementations.entity;


 import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;

import com.villagecraft.framework.mob.util.CustomEntityTag;
import com.villagecraft.framework.toolset.nbt.NBT;

import net.minecraft.server.v1_12_R1.EntityZombie;
import net.minecraft.server.v1_12_R1.NBTTagCompound;


 public abstract class CEHusk extends EntityZombie implements CustomEntityTag
 {
	 public static final int ID = EntityType.ZOMBIE.getTypeId();

	
	 public CEHusk(World world) {
		 super(world);
		 NBTTagCompound compound = NBT.copyTagFromEntity((Zombie) this, null);
		 compound.setByte("ZombieType", (byte) 6);
		 NBT.applyTagToEntity(compound, (Zombie) this);
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.mob.implementations.entity.CEHusk JD-Core Version:
 * 0.6.2
 */