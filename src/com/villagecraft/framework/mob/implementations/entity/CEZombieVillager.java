 package com.villagecraft.framework.mob.implementations.entity;


 import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;

import com.villagecraft.framework.mob.util.CustomEntityTag;
import com.villagecraft.framework.toolset.nbt.NBT;

import net.minecraft.server.v1_12_R1.EntityZombie;
import net.minecraft.server.v1_12_R1.NBTTagCompound;


 public abstract class CEZombieVillager extends EntityZombie implements CustomEntityTag
 {
	 public static final int ID = EntityType.ZOMBIE.getTypeId();

	
	 public CEZombieVillager(World world) {
		 super(world);
		 setZombieVillagerType(ZombieVillagerType.FARMER);
		 }

	
	 public void setZombieVillagerType(ZombieVillagerType type) {
		 NBTTagCompound compound = NBT.copyTagFromEntity((Zombie) this, null);
		 compound.setByte("ZombieType", type.getValue());
		 NBT.applyTagToEntity(compound, (Zombie) this);
		 }

	
	 public static enum ZombieVillagerType {
		 FARMER(1),  LIBRARIAN(2),  PRIEST(3),  BLACKSMITH(4),  BUTCHER(5);
		
		 private int value;

		
		 private ZombieVillagerType(int val) {
			this.value = val;
		}

		
		 public byte getValue()
		 {
			 return (byte) this.value;
			 }
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.mob.implementations.entity.CEZombieVillager
 * JD-Core Version: 0.6.2
 */