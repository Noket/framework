package com.villagecraft.framework.mob.implementations.entity.behavior.attack;

public interface AttackStyle {
	
	public void attack();
}
