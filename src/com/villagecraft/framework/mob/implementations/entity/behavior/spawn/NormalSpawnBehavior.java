 package com.villagecraft.framework.mob.implementations.entity.behavior.spawn;


 import org.bukkit.Location;

import com.villagecraft.framework.mob.interfaces.entity.IndividualBehavior;


 public class NormalSpawnBehavior implements IndividualBehavior
 {
	 Entity entity;
	 Location location;

	
	 public NormalSpawnBehavior(Entity entity, Location location)
	 {
		 this.entity = entity;
		 this.location = location;
		 }

	
	 public boolean behave()
	 {
		 return true;
		 }

	
	 public void setComplete(boolean value)
	 {
		 }

	
	 public void setRequeue(boolean value)
	 {
		 }

	
	 public boolean isComplete()
	 {
		 return false;
		 }

	
	 public boolean shouldRequeue()
	 {
		 return false;
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.mob.implementations.entity.behavior.spawn.
 * NormalSpawnBehavior JD-Core Version: 0.6.2
 */