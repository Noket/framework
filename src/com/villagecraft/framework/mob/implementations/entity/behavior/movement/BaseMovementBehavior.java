package com.villagecraft.framework.mob.implementations.entity.behavior.movement;

import com.villagecraft.framework.mob.interfaces.entity.IndividualBehavior;

public abstract class BaseMovementBehavior implements IndividualBehavior {

	public boolean requeue = false;
	public boolean complete = false;
	
	public void setComplete(boolean value) {
		complete = value;
	}

	public void setRequeue(boolean value) {
		requeue = value;
	}

	public boolean isComplete() {
		return complete;
	}

	public boolean shouldRequeue() {
		return requeue;
	}

}
