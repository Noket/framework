package com.villagecraft.framework.mob.implementations.entity.behavior.animation;

public class NoAnimationBehavior extends BaseAnimationBehavior{
	
	public boolean behave() {
		animate();
		return true;
	}

	@Override
	public void animate() {
		return;
	}


}
