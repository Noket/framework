 package com.villagecraft.framework.mob.implementations.entity;


 import org.bukkit.entity.EntityType;
import org.bukkit.entity.Guardian;

import com.villagecraft.framework.mob.util.CustomEntityTag;
import com.villagecraft.framework.toolset.nbt.NBT;

import net.minecraft.server.v1_12_R1.EntityGuardian;
import net.minecraft.server.v1_12_R1.NBTTagCompound;


 public abstract class CEElderGuardian extends EntityGuardian implements CustomEntityTag
 {
	 public static final int ID = EntityType.GUARDIAN.getTypeId();

	
	 public CEElderGuardian(World world) {
		 super(world);
		 NBTTagCompound compound = NBT.copyTagFromEntity((Guardian) this, null);
		 compound.setBoolean("Elder", true);
		 NBT.applyTagToEntity(compound, (Guardian) this);
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.mob.implementations.entity.CEElderGuardian JD-Core
 * Version: 0.6.2
 */