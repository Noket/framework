 package com.villagecraft.framework.mob.implementations.entity;


 import org.bukkit.entity.EntityType;
import org.bukkit.entity.Skeleton;

import com.villagecraft.framework.mob.util.CustomEntityTag;
import com.villagecraft.framework.toolset.nbt.NBT;

import net.minecraft.server.v1_12_R1.EntitySkeleton;
import net.minecraft.server.v1_12_R1.NBTTagCompound;


 public abstract class CEStray extends EntitySkeleton implements CustomEntityTag
 {
	 public static final int ID = EntityType.SKELETON.getTypeId();

	
	 public CEStray(World world) {
		 super(world);
		 NBTTagCompound compound = NBT.copyTagFromEntity((Skeleton) this, null);
		 compound.setByte("SkeletonType", (byte) 2);
		 NBT.applyTagToEntity(compound, (Skeleton) this);
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.mob.implementations.entity.CEStray JD-Core
 * Version: 0.6.2
 */