package com.villagecraft.framework.mob.util;

public abstract class GroupSizeProperty {
	private boolean executionComplete;

	public abstract boolean executionIsComplete();

	public abstract int getSubunitCount();
}

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.mob.util.GroupSizeProperty JD-Core Version: 0.6.2
 */