package com.villagecraft.framework.mob.util;

public abstract class GroupMobilityAction {
	private boolean executionComplete;

	public abstract void move();

	public abstract boolean executionIsComplete();
}

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.mob.util.GroupMobilityAction JD-Core Version:
 * 0.6.2
 */