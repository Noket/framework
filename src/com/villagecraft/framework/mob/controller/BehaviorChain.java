package com.villagecraft.framework.mob.controller;

import net.minecraft.server.v1_12_R1.EntityLiving;

public abstract class BehaviorChain {

	private EntityLiving entityLiving;

	public BehaviorChain(net.minecraft.server.v1_12_R1.EntityLiving livingEntity) {
		entityLiving = livingEntity;

	}

	public abstract BehaviorChain behave();
}
