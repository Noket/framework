package com.villagecraft.framework.mob.interfaces.entity;

public abstract interface IndividualBehavior {
	

	
	public abstract boolean behave();

	public abstract void setComplete(boolean paramBoolean);

	public abstract void setRequeue(boolean paramBoolean);

	public abstract boolean isComplete();

	public abstract boolean shouldRequeue();
}

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.mob.interfaces.entity.IndividualBehavior JD-Core
 * Version: 0.6.2
 */