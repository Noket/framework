package com.villagecraft.framework;

public enum Sentinel {
	
	ENABLING,
	LOADING,
	RUNNING,
	DISABLING,
	COMMANDING;

	public static Sentinel STATE;

	/**
	 * This method is the preferred way to log information for both troubleshooting and readability reasons.
	 * <p><p>Just pass the message, and the "this" operator (so that the ExtensibleDelegate can report which branch sent the message)
	 * @param str : The string to be output
	 * @param clazz : The class reporting the information
	 */
	public final void log(String str, Class<?> clazz)
	{
		switch(STATE)
		{
		case DISABLING:
			System.out.println("        [Disable]["+clazz.getSimpleName()+"] "+str);
			break;
		case ENABLING:
			System.out.println("        [Enable]["+clazz.getSimpleName()+"] "+str);
			break;
		case LOADING:
			System.out.println("        [Load]["+clazz.getSimpleName()+"] "+str);
			break;
		case RUNNING:
			System.out.println("        [Run]["+clazz.getSimpleName()+"] "+str);
			break;
		case COMMANDING:
			System.out.println("        [Command]["+clazz.getSimpleName()+"] "+str);
			break;
		default:
			System.out.println("        ["+clazz.getSimpleName()+"]"+str);
			break;
		}
	}
	
	public final void log(String str)
	{
		STATE = Framework.delegateState;
		switch(STATE)
		{
		case DISABLING:
			System.out.println("        [Disable] "+str);
			break;
		case ENABLING:
			System.out.println("        [Enable] "+str);
			break;
		case LOADING:
			System.out.println("        [Load] "+str);
			break;
		case RUNNING:
			System.out.println("        [Run] "+str);
			break;
		case COMMANDING:
			System.out.println("        [Command] "+str);
			break;
		default:
			System.out.println("        "+str);
			break;
		}
	}
}
