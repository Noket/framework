package com.villagecraft.framework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.reflect.Reflection;
import com.villagecraft.framework.animation.PlayerJoinSplashAnimation;
import com.villagecraft.framework.events.CustomEventManager;
import com.villagecraft.framework.exceptions.FailedDisableException;
import com.villagecraft.framework.exceptions.FailedEnableException;
import com.villagecraft.framework.exceptions.FailedLoadException;
import com.villagecraft.framework.toolset.Toolset;

/**
 * A class that delegates the methods onEnable(), onDisable(), onLoad(), and onCommand() found in JavaPlugin. 
 * To interact with this class, a branch must implement PluginBranch's methods and the branch
 * must be registered with this Framework (via Framework.registerBranch()) in your JavaPlugin's "static" field.
 * <br><p>
 * ie:<br>
 * static{<br>
 * Framework.registerBranch(HelloWorldBranch.class);
 * <br>}
 * <br><br>
 * It's important to register in the static section of the plugin, because this ensures the objects are properly created prior to running onEnable or any other methods.
 * This class will also allow dynamic registration of commands by writing 
 * commands directly to the plugin.yml file, however this is not yet implemented.
 * 
 * @author james
 *
 */
public class Framework extends JavaPlugin {
	public static final Toolset TOOLSET = new Toolset();
	public static final CustomEventManager EVENT_MANAGER;
	private static final Framework FW = new Framework();
	private static HashMap<Class<? extends PluginBranch>, PluginBranch> constructedObjectMap = new HashMap<>(); 
	private static HashSet<Class<? extends PluginBranch>> registeredClasses = new HashSet<>();
	
	protected ArrayList<Player> onlinePlayers = new ArrayList<Player>();

	//private static HashSet<Class<? extends PluginDelegatable>> unlockedClasses = new HashSet<>();
	protected static Sentinel delegateState = Sentinel.LOADING;
	//private static final String PREFIX = ""+ChatColor.BOLD+ChatColor.DARK_RED+"$/: "+ChatColor.RESET;
	
	//private static Database database = new Database();
	//private static DatabaseFile databaseFile = database.createDatabaseFile(DatabaseType.YAML, "BranchManager", "branches");
	//private static YamlConfiguration yamlConfig = databaseFile.getYamlDatabase();
	
	
	static {
		EVENT_MANAGER = CustomEventManager.getSingleton();
	}
	
	public enum BannerType{
		 HEADER(" >> VillageCraft Framework - Plugin Content Start <<"),
		 FOOTER(" >>  VillageCraft Framework - Plugin Content End  <<"),
		DIVIDER(" ::  ===============================  ::");
		
		private final String value;
		BannerType(String str){
			value = str;
		}
		
		public void printValue(){
			System.out.println(value);
		}
	}
	
	public final static Sentinel logger() {
		return delegateState;
	}
	
	private final static void formatAndPrintException(Exception ex, Object faultyobject){
		if(faultyobject!=null){
			delegateState.log("> ["+faultyobject.getClass().getSimpleName()+"] Exception thrown!");
		}else{
			delegateState.log("> Exception thrown!");
		}
		System.out.println("  =: Begin Exception Print :=");
		StackTraceElement[] elements = ex.getStackTrace();
		for(StackTraceElement element : elements){
			System.out.println("   "+element.toString());
		}
		System.out.println("  =: End Exception Print :=");	
	}
	
	/**
	 * <p>Registers the branch with the PluginDelegate. Using this command in the main Plugin will register
	 * the branches for construction at runtime. All constructed classes will have event listeners registered.

	 * @param clazz - The class to be registered
	 */
	public final static void registerBranch(Class<? extends PluginBranch> clazz)
	{
		registeredClasses.add(clazz);
	}

	public final static void commandBranches(CommandSender sender, Command command, String label, String[] arguments)
	{ 
		
		delegateState = Sentinel.COMMANDING;
		
		Iterator<Class<? extends PluginBranch>> registeredClassIterator = registeredClasses.iterator();
		while(registeredClassIterator.hasNext())
		{
			Class<? extends PluginBranch> registeredClass = registeredClassIterator.next();
			PluginBranch constructedObject = constructedObjectMap.get(registeredClass);
			
			if(constructedObject != null)
			{
				try{
			constructedObject.command(sender, command, label, arguments);
				}
				catch(NullPointerException exception)
				{
					exception.printStackTrace();
					continue;
				}
			}
		}
		
		delegateState = Sentinel.RUNNING;
	}
	
	/**
	 * <p>Disables all registered branches. This is analogous to "onDisable()" command specified by JavaPlugin
	 */
	public final static void disableBranches()
	{
		delegateState = Sentinel.DISABLING;

		//System.out.println(":::::::::::::: PluginDelegate ::::::::::::::");

		System.out.println("     > Disabling branches.. <");

		Iterator<Class<? extends PluginBranch>> registeredClassIterator = registeredClasses.iterator();
		while(registeredClassIterator.hasNext())
		{
			Class<? extends PluginBranch> registeredClass = registeredClassIterator.next();
			PluginBranch constructedObject = constructedObjectMap.get(registeredClass);
			
			if(constructedObject != null)
			{
				try {
					delegateState.log("["+constructedObject.getClass().getSimpleName()+"] Disabling branch ..",constructedObject.getClass());
					constructedObject.disable();
					delegateState.log("["+constructedObject.getClass().getSimpleName()+"] Branch disabled.",constructedObject.getClass()); 
				}
					catch(NullPointerException | FailedDisableException exception){
					delegateState.log("["+constructedObject.getClass().getSimpleName()+"] Exception thrown!",constructedObject.getClass());
					exception.printStackTrace();
					continue;
				}
			}
		}
		System.out.println("     > Branches disabled. <");
		//System.out.println(":::::::::::::: PluginDelegate ::::::::::::::");
	}
	
	/**
	 * Gets the instance of a class which is mapped within the PluginDelegate. Classes which call this method should check for null, because a branch may not have been constructed as anticipated.
	 * Classes which use this method will be forced to cast the returned object to whichever class you need. No way around this in Java, sorry.
	 * @author James Sola
	 * @param clazz - The "object.class" that you need
	 * @return The constructed object
	 */
	public static <C extends PluginBranch> PluginBranch getClassInstance(Class<C> clazz)
	{
		return constructedObjectMap.get(clazz);
	}
	
	public void onLoad() {

		//System.out.println(":::::::::::::: PluginDelegate ::::::::::::::");
		System.out.println("     > loadBranches() called.. <");
		System.out.println("     > Initiating branch construction.. <");

		Set<Class<? extends PluginBranch>> nullKeyValues = new HashSet<>();
		Iterator<Class<? extends PluginBranch>> registeredClassIterator = registeredClasses.iterator();

		while(registeredClassIterator.hasNext())
		{
			Class<? extends PluginBranch> nextClass = registeredClassIterator.next();
			PluginBranch nextObject = constructedObjectMap.get(nextClass);
			
			if(nextObject == null)
			{
				nullKeyValues.add(nextClass);
			}
		}
		
		Iterator<Class<? extends PluginBranch>> nullValueIterator = nullKeyValues.iterator();
		Set<PluginBranch> objectsNotLoaded = new HashSet<>();
		PluginBranch constructedObject = null;
		
		if(!nullValueIterator.hasNext())
		{
			System.out.println("     > No branches need construction. <");
		}
		
		while(nullValueIterator.hasNext())
		{
			Class<? extends PluginBranch> registeredClass = nullValueIterator.next();
			constructedObject = constructedObjectMap.get(registeredClass);
			try 
			{
				System.out.println("       ↳ ["+constructedObject.getClass().getSimpleName()+"] Branch constructed.");
				constructedObject = registeredClass.newInstance();
				objectsNotLoaded.add(constructedObject);
				constructedObjectMap.put(constructedObject.getClass(),constructedObject);
				
			} 
			catch (InstantiationException | IllegalAccessException | IllegalArgumentException e) 
			{
				System.out.println("       ↳ ["+constructedObject.getClass().getSimpleName()+"] !! Could not construct branch !!");
			
				constructedObjectMap.remove(registeredClass);
				registeredClasses.remove(registeredClass);
			}	
		}	
		
		Iterator<PluginBranch> notLoadedObjectIterator = objectsNotLoaded.iterator();
		System.out.println("     > Loading branches.. <");
		delegateState = Sentinel.LOADING;

		while(notLoadedObjectIterator.hasNext())
		{
			PluginBranch loadedObject = notLoadedObjectIterator.next();
			
			if(loadedObject != null)
			{
				try{
					delegateState.log("> ["+loadedObject.getClass().getSimpleName()+"] Loading branch ..",loadedObject.getClass());
					loadedObject.load();
					delegateState.log("> ["+loadedObject.getClass().getSimpleName()+"] Branch loaded.",loadedObject.getClass());
				}
				catch (NullPointerException | FailedLoadException exception) {
					delegateState.log("> ["+loadedObject.getClass().getSimpleName()+"] Exception thrown!",loadedObject.getClass());
					exception.printStackTrace();
					continue;
				}
			}
		}
		
		System.out.println("     > loadBranches() complete. <");

	}

	public void onEnable() {
		
		CustomEventManager.registerListeners();
		
		System.out.println("     > enableBranches() called.. <");
		System.out.println("     > Initiating branch construction.. <");

		Set<Class<? extends PluginBranch>> nullKeyValues = new HashSet<>();

		Iterator<Class<? extends PluginBranch>> registeredClassIterator = registeredClasses.iterator();
		
		//Get any null values, meaning any object that haven't been constructed
		
		while(registeredClassIterator.hasNext())
		{
			Class<? extends PluginBranch> nextClass = registeredClassIterator.next();
			
			PluginBranch nextObject = constructedObjectMap.get(nextClass);
			
			if(nextObject == null)
			{
				nullKeyValues.add(nextClass);
			}
		}
		
		//Create an iterator that iterates through the non-constructed objects
		
		Iterator<Class<? extends PluginBranch>> nullValueIterator = nullKeyValues.iterator();		
		
		Set<PluginBranch> objectsNotEnabled = new HashSet<>();
		PluginBranch constructedObject = null;

		if(!nullValueIterator.hasNext())
		{
			System.out.println("     > No branches need construction <");
		}
		
		
		//Construct them, and add them to a list which will be used to run enable()
		
		while(nullValueIterator.hasNext())
		{
			Class<? extends PluginBranch> registeredClass = nullValueIterator.next();

			try 
			{
				constructedObject = registeredClass.newInstance();
				objectsNotEnabled.add(constructedObject);
				constructedObjectMap.put(constructedObject.getClass(), constructedObject);
				System.out.println("       ↳ ["+constructedObject.getClass().getSimpleName()+"] Branch constructed.");
			} 
			catch (InstantiationException | IllegalAccessException | IllegalArgumentException | SecurityException e){
				System.out.println("       ↳ ["+constructedObject.getClass().getSimpleName()+"] !! Could not load branch !!");
			}
		}
		
		//Create an iterator for the set, and enable them all.
		
		Iterator<PluginBranch> notEnabledObjectIterator = objectsNotEnabled.iterator();
		System.out.println("     > Enabling branches.. <");
		delegateState = Sentinel.ENABLING;

		while(notEnabledObjectIterator.hasNext())
		{
			PluginBranch enabledObject = notEnabledObjectIterator.next();
			
			if(enabledObject != null)
			{
				
					try{
						delegateState.log("> ["+enabledObject.getClass().getSimpleName()+"] Enabling branch ..",enabledObject.getClass());
						enabledObject.enable();
						delegateState.log("> ["+enabledObject.getClass().getSimpleName()+"] Branch enabled.",enabledObject.getClass());
					}
				catch (NullPointerException | FailedEnableException exception) {
					delegateState.log("> ["+constructedObject.getClass().getSimpleName()+"] Exception thrown!",constructedObject.getClass());
					exception.printStackTrace();
					
					constructedObjectMap.remove(enabledObject.getClass());
					registeredClasses.remove(enabledObject.getClass());
					continue;
				}
			}
		}
		
		//Finally, register
		
		Iterator<Class<? extends PluginBranch>> registerIterator = registeredClasses.iterator();
		System.out.println("     > Registering branch event handlers.. <");

		while(registerIterator.hasNext())
		{
			Class<? extends PluginBranch> registeredClass = registerIterator.next();
			PluginBranch object = constructedObjectMap.get(registeredClass);
			
			//if(object != null && branchIsUnlocked(registeredClass))
			//{
				System.out.println("       ↳ ["+object.getClass().getSimpleName()+"] Branch events registered.");
				Bukkit.getPluginManager().registerEvents(object, this);
			//}
			//else if(!branchIsUnlocked(registeredClass))
			//{
			//	System.out.println("       ↳ ["+object.getClass().getSimpleName()+"] Branch is locked. Events not registered.");
		//	}
		}
		delegateState = Sentinel.RUNNING;
		//System.out.println(":::::::::::::: PluginDelegate ::::::::::::::");
	}

	public static final Framework getSingleton() {
		return FW;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		onlinePlayers.add(event.getPlayer());
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		onlinePlayers.remove(event.getPlayer());
	}
}
