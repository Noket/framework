package com.villagecraft.framework.database;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.bukkit.configuration.file.YamlConfiguration;

import com.villagecraft.framework.database.Databasable.DatabaseType;
import com.villagecraft.framework.exceptions.ImproperDatabaseException;

/**
 * <p>This Database helper class creates a database by accepting a foldername when the object is created.
 * After doing so, you can access the folder path or the path to the database.yml file.
 * This object also allows easy creation of any database by passing first the foldername, then the filename.
 * @author james
 *
 */
public class Database
{

	private String folderPath;
	private String filePath;
	private static String databasePath = "plugins/Database";
	private DatabaseFile databaseFile;
	private DatabaseType databaseType;
	private Database database;
		
	private Set<DatabaseFile> loadedFiles;
	
	/**
	 * Creates a new database with a standardized folder system. <br>
	 * @param folderName - name of folder in which to store database.yml
	 */
	public Database()
	{
		database = this;
		loadedFiles = new HashSet<>();
	}
	
	/**
	 * Returns DatabaseFiles that match the DatabaseType requested. If none are found, an empty set is returned.
	 * @return
	 */
	public Set<DatabaseFile> getDatabaseFile(DatabaseType type)
	{
		Set<DatabaseFile> matchingFiles = Collections.emptySet();
		Iterator<DatabaseFile> loadedFileIterator = loadedFiles.iterator();
		while(loadedFileIterator.hasNext())
		{
			DatabaseFile next = loadedFileIterator.next();
			if(!type.equals(next.getDatabaseType()))
			{
				continue;
			}
			else if (matchingFiles.isEmpty())
			{
				matchingFiles = new HashSet<>();
				matchingFiles.add(next);
			}
			else
			{
				matchingFiles.add(next);
			}
		}
		
		return matchingFiles;
	}
	
	/** <b>Description:</b>
	 * <p>Gets the {@link DatabaseFile} by the name of the file, excluding suffix (ie: "database"). 
	 * If no file could be found, null is returned. Note: This method is case sensitive.
	 * @param fileName - Name of the file to search for
	 * @return A matching DatabaseFile, or null
	 * @author james
	 */
	public DatabaseFile getDatabaseFile(String fileName)
	{
		DatabaseFile matchingFile = null;
		Iterator<DatabaseFile> loadedFileIterator = loadedFiles.iterator();
		while(loadedFileIterator.hasNext())
		{
			DatabaseFile next = loadedFileIterator.next();
			if(!fileName.equals(next.getFile().getName()))
			{
				continue;
			}
			else
			{
				matchingFile = next;
				break;
			}
		}
		
		return matchingFile;
	}
	
	/**
	 * Creates a DatabaseFile, allowing the enum to determine how the file should be created and saved
	 * @param databaseType - An Enumerated DatabaseType.
	 * @param folderName - The name of the folder where the file should be saved (correct string format: "foldername")
	 * @param filePath - The name of the file, excluding suffix (correct string format: "filename")
	 * @return The file created and saved in the DatabaseType, for chaining (if desired).
	 */
	public DatabaseFile createDatabaseFile(DatabaseType databaseType, String folderName, String fileName, boolean obfuscated)
	{		
		DatabaseFile file = new DatabaseFile(databaseType, folderName, fileName, obfuscated);
		loadedFiles.add(file);
		file.save();
		return file;
	}
	
	/**
	 * Creates a new DatabaseFile, stored within this Database object. Calling this method will create an actual physical file. To save
	 * any changes to the file, run ".save()" on the DatabaseFile that was created.
	 * 
	 * @return DatabaseFile - The DatabaseFile created
	 */
	public DatabaseFile createDatabaseFile(DatabaseType databaseType, String folderName, String fileName)
	{
		DatabaseFile file = new DatabaseFile(databaseType, folderName, fileName);
		loadedFiles.add(file);
		file.save();
		return file;
	}
	
	/**
	 * Creates a new DatabaseFile, stored within this Database object. Calling this method will create an actual physical file. To save
	 * any changes to the file, run ".save()" on the DatabaseFile that was created.
	 * 
	 * @param databaseType - The type of database you want
	 * @param folderName - The name of the folder
	 * @param subFolderName - The name of the sub-folder
	 * @param fileName - The name of the file. Don't include suffix.
	 * @return The DatabaseFile created
	 */
	public DatabaseFile createDatabaseFile(DatabaseType databaseType, String folderName, String subFolderName, String fileName) 
	{
		DatabaseFile file = new DatabaseFile(databaseType, folderName, subFolderName, fileName);
		loadedFiles.add(file);
		file.save();
		return file;
	}
	/**
	 * Loads a file configured to the database selected.
	 * @param file - File to be parsed
	 * @return Whether or not the file was loaded correctly
	 */
	public boolean loadDatabaseFile(DatabaseFile file) throws ImproperDatabaseException {
		switch (file.getDatabaseType()) {
		case YAML:
			YamlConfiguration.loadConfiguration(file.getFile());
			return true;
		default:
			throw new ImproperDatabaseException();
		}
	}
	
	/**
	 * Sets the database default path to something else. By default, this is "plugins/Database".
	 * <br><br>
	 * <b>Note:</b> Changing this affects all databases saved. A database location or name should be
	 * chosen and then left alone, unless you want to manually move your database files to a new
	 * location.
	 * @param path
	 */
	public static void setBasePath(String path)
	{
		databasePath = path;
	}
	
	/**
	 * Gets the base pathway where all databases will be stored. This path will be consistent for all databases created. By default, this is "plugins/Database".
	 * @param path
	 * @return Base pathway to all databases
	 */
	public static String getBasePath()
	{
		return databasePath;
	}
}
