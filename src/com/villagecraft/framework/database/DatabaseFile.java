package com.villagecraft.framework.database;

import java.io.File;
import java.io.IOException;

import lib.PatPeter.SQLibrary.MySQL;
import lib.PatPeter.SQLibrary.PostgreSQL;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.libs.joptsimple.internal.Strings;
import org.sqlite.SQLite;

import com.villagecraft.framework.Framework;

public final class DatabaseFile extends File implements Databasable{

	private static final long serialVersionUID = 3993626995063136266L;
	private static String databasePath = Database.getBasePath();
	
	
	private DatabaseType databaseType;
	private File file;

	private YamlConfiguration yaml;
	private SQLite sqlite;
	private MySQL mysql;
	private PostgreSQL postgre;

	@SuppressWarnings("unused")
	private String folderPath, folderName, filePath, fileName;

	@SuppressWarnings("unused")
	private boolean read, write, obfuscated;

	/**
	 * Creates a database file with read/write permissions.
	 * 
	 * @param databaseType
	 *            - What type of database this file should conform to
	 * @param folderName
	 *            - The name of the folder where this database should be stored.
	 * @param fileName
	 *            - Name of the file (excluding suffix) to be created.
	 * @param obfuscated
	 *            - Whether the saved database should be difficult to interpret (not yet implemented)
	 */
	protected DatabaseFile(DatabaseType databaseType, String folderName, String fileName, boolean obfuscated) {
		super(databasePath + "/" + databaseType.getDatabaseFolderName() + "/" + folderName + "/" + fileName + databaseType.getFileType());
		this.file = this;
		this.databaseType = databaseType;
		
		this.folderName = folderName;
		this.folderPath = databasePath + "/" + databaseType.getDatabaseFolderName() + "/" + folderName + "/";

		this.fileName = fileName;
		this.filePath = folderPath + this.fileName + this.databaseType.getFileType();
		
		this.obfuscated = obfuscated;

		switch (databaseType){
		case YAML :
			yaml = YamlConfiguration.loadConfiguration(file);
			try {
				yaml.save(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("yaml db found");
			break;
		case SQLITE :
			break;
		case MYSQL :
			break;
		case XML :
			break;
		case POSTGRE :
			break;
		case ORACLE :
			break;
		default:
			break;
		}
		Framework.logger().log("DatabaseFile Created: " + this.filePath, this.getClass());
	
	}

	
	/**
	 * Creates a database file with read/write permissions.
	 * 
	 * @param databaseType
	 *            - What type of database this file should conform to
	 * @param folderName
	 *            - The name of the folder where this database should be stored.
	 * @param fileName
	 *            - Name of the file (excluding suffix) to be created.
	 */
	protected DatabaseFile(DatabaseType databaseType, String folderName, String fileName) {
		super(databasePath + "/" + databaseType.getDatabaseFolderName() + "/" + folderName + "/" + fileName + databaseType.getFileType());
		this.file = this;
		this.databaseType = databaseType;
		
		this.folderName = folderName;
		this.folderPath = databasePath + "/" + databaseType.getDatabaseFolderName() + "/" + this.folderName + "/";

		this.fileName = fileName;
		this.filePath = folderPath + this.fileName + this.databaseType.getFileType();
		switch (databaseType){
		case YAML :
			yaml = YamlConfiguration.loadConfiguration(file);
			//yaml.set("HAHA", 12);
			try {
				yaml.save(getFile());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("yaml db found");
			break;
		case SQLITE :
			break;
		case MYSQL :
			break;
		case XML :
			break;
		case POSTGRE :
			break;
		case ORACLE :
			break;
		default:
			break;
		}
		Framework.state().log("DatabaseFile Created: " + this.filePath, this.getClass());
	}
	
	protected DatabaseFile(DatabaseType databaseType, String folderName, String subFolderName, String fileName) {
		super(databasePath + "/" + databaseType.getDatabaseFolderName() + "/" + folderName + "/" + subFolderName + "/" + fileName + databaseType.getFileType());
		this.file = this;
		this.databaseType = databaseType;
		
		this.folderName = folderName;
		this.folderPath = databasePath + "/" + databaseType.getDatabaseFolderName() + "/" + this.folderName + "/" + subFolderName + "/";

		this.fileName = fileName;
		this.filePath = folderPath + this.fileName + this.databaseType.getFileType();
		switch (databaseType){
		case YAML :
			yaml = YamlConfiguration.loadConfiguration(file);
			//yaml.set("HA", 12);
			try {
				yaml.save(getFile());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//System.out.println("yaml db found");
			break;
		case SQLITE :
			break;
		case MYSQL :
			break;
		case XML :
			break;
		case POSTGRE :
			break;
		case ORACLE :
			break;
		default:
			break;
		}
		Framework.logger().log("DatabaseFile Created: " + this.filePath, this.getClass());
	}
	
	public String getFileType()
	{
		switch(databaseType)
		{
		case YAML:
			return ".yml";
		default:
			return Strings.EMPTY;
		}
	}
	
	public void save(){
		switch (databaseType) {
		case YAML:
			
			Framework.logger().log("Save called!", this.getClass());
			
			try {
				yaml.save(getFile());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Gets the type of database for which the file is configured.
	 * 
	 * @return
	 */
	public DatabaseType getDatabaseType() {
		return databaseType;
	}

	public YamlConfiguration getYamlDatabase() {
		return yaml;
	}

	public SQLite getSQLiteDatabase() {
		return sqlite;
	}

	public MySQL getMySQLDatabase() {
		return mysql;
	}

	public PostgreSQL getPostGREDatabase() {
		return postgre;
	}
	
	public File getFile()
	{
		return file;
	}
}
