package com.villagecraft.framework.database;

import lib.PatPeter.SQLibrary.MySQL;
import lib.PatPeter.SQLibrary.PostgreSQL;

import org.bukkit.configuration.file.YamlConfiguration;
import org.sqlite.SQLite;

import com.villagecraft.framework.exceptions.ImproperDatabaseException;

public interface Databasable
{	
	public enum DatabaseType
	{	
		YAML{
			public String getDatabaseFolderName()
			{
				return "YAML";
			}

			public String getFileType() {
				return ".yml";
			}
		},
		SQLITE{
			public String getDatabaseFolderName()
			{
				return "SqLite";
			}

			public String getFileType() {
				return ".sql";
			}
		},
		MYSQL{
			public String getDatabaseFolderName()
			{
				return "MySQL";
			}

			//TODO: Figure out what the file extension is
			public String getFileType() {
				return null;
			}
		},
		XML{
			public String getDatabaseFolderName()
			{
				return "XML";
			}

			public String getFileType() {
				return ".xml";
			}
		},
		POSTGRE{
			public String getDatabaseFolderName()
			{
				return "PostGRE";
			}

			//TODO: Figure out what the file extension is
			public String getFileType() {
				return null;
			}
		},
		ORACLE{
			public String getDatabaseFolderName()
			{
				return "Oracle";
			}

			//TODO: Figure out what the file extension is
			public String getFileType() {
				return null;
			}
		};
		
		/**
		 * Returns the name of the folder, without any formatting (forward slashes)
		 * @return The bare folder name
		 */
		public abstract String getDatabaseFolderName();
		
		public abstract String getFileType();
	}
	
	/**
	 * Gets the YamlConfiguration saved in the database. <Br><br>
	 * <i>Note:</i> This will return null if no YamlConfiguration exists.
	 * <br>
	 */
	public YamlConfiguration getYamlDatabase();
	
	/**
	 * Gets the YamlConfiguration saved in the database. <Br><br>
	 * <i>Note:</i> This will return null if no YamlConfiguration exists.
	 * <br>
	 */
	public SQLite getSQLiteDatabase();

	/**
	 * Gets the YamlConfiguration saved in the database. <Br><br>
	 * <i>Note:</i> This will return null if no YamlConfiguration exists.
	 * <br>
	 */
	public MySQL getMySQLDatabase();
	//public Xml getXMLDatabase();
	
	/**
	 * Gets the PostgreSql file saved in the database. <Br><br>
	 * <i>Note:</i> This will return null if no PostgreSql exists.
	 * <br>
	 */
	public PostgreSQL getPostGREDatabase();
	//public OracleTable getOracleDatabase();
		
	/**
	 * Saves the DatabaseFile from memory to the disk.
	 * <br><br>
	 * This Enum's implementation will handle all DatabaseTypes appropriately. No need to worry about database specific syntax.
	 * @throws ImproperDatabaseException 
	 */
	public abstract void save() throws ImproperDatabaseException;
	
	/**
	 * Returns the DatabaseType
	 * @return DatabaseType
	 */
	public abstract DatabaseType getDatabaseType();

}

