 package com.villagecraft.framework.inventory;


 import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.HashBiMap;
import com.villagecraft.framework.Framework;
import com.villagecraft.framework.exceptions.IllegalSizeException;
import com.villagecraft.framework.exceptions.InventoryNotFoundException;


 public abstract class Menu implements Listener
 {
	 private final UUID uniqueMenuID;
	 private final HashBiMap<Integer, Inventory> INVENTORY = HashBiMap.create();
	 private int currentPage = 0;

	
	 public Menu() {
		 this.uniqueMenuID = UUID.randomUUID();
		
		 MenuTracker.registerMenu(this, this.uniqueMenuID);
		 Bukkit.getPluginManager().registerEvents(this, Framework.getSingleton());
		 }

	
	 public Menu(UUID menuID) {
		 this.uniqueMenuID = menuID;
		
		 MenuTracker.registerMenu(this, this.uniqueMenuID);
		 Bukkit.getPluginManager().registerEvents(this, Framework.getSingleton());
		 }

	
	 public final UUID getMenuID() {
		 return this.uniqueMenuID;
		 }

	
	 public synchronized boolean containsInventory(Inventory inv) {
		 Set<Inventory> pages = this.INVENTORY.values();
		 return pages.contains(inv);
		 }

	
	 public void addPage(int size) throws IllegalSizeException
	 {
		 if (size % 9 == 0) {
			 if (!this.INVENTORY.containsKey(Integer.valueOf(0)))
				 this.INVENTORY.put(Integer.valueOf(this.INVENTORY.size()),
						Bukkit.createInventory(null, size, "Page " + (this.INVENTORY.size() + 1)));
			 else this.INVENTORY.put(Integer.valueOf(this.INVENTORY.size() + 1),
					Bukkit.createInventory(null, size, "Page " + (this.INVENTORY.size() + 1)));
			 }
		 else throw new IllegalSizeException();
		 }

	
	 public void addPage(int size, String name) throws IllegalSizeException
	 {
		 if (size % 9 == 0) {
			 if (!this.INVENTORY.containsKey(Integer.valueOf(0))) this.INVENTORY.put(Integer.valueOf(0),
					Bukkit.createInventory(null, size, "Page " + (this.INVENTORY.size() + 1)));
			 else this.INVENTORY.put(Integer.valueOf(this.INVENTORY.size() + 1),
					Bukkit.createInventory(null, size, "Page " + (this.INVENTORY.size() + 1) + ", " + name));
			 }
		 else throw new IllegalSizeException();
		 }

	
	 public void addPage(String name, InventoryType type)
	 {
		 if (!this.INVENTORY.containsKey(Integer.valueOf(0))) this.INVENTORY.put(Integer.valueOf(0),
				Bukkit.createInventory(null, type, "Page " + (this.INVENTORY.size() + 1)));
		 else this.INVENTORY.put(Integer.valueOf(this.INVENTORY.size() + 1),
				Bukkit.createInventory(null, type, "Page " + (this.INVENTORY.size() + 1) + ", " + name));
		 }

	
	 public Inventory getCurrentPage()
	 {
		 return (Inventory) this.INVENTORY.get(Integer.valueOf(this.currentPage));
		 }

	
	 public int getPageNumber() {
		 return this.currentPage;
		 }

	
	 public void goForward() {
		 if (this.currentPage < 128) this.currentPage = (this.currentPage++);
		 }

	
	 public void goBackward()
	 {
		 if (this.currentPage > 0) this.currentPage -= 1;
		 }

	
	 public int getInventoryCollectionSize()
	 {
		 return this.INVENTORY.size();
		 }

	
	 public static ItemStack addLoreLine(ItemStack item, String string)
	 {
		 if (item.hasItemMeta()) {
			 ItemMeta meta = item.getItemMeta();
			 if (meta.hasLore()) {
				 List loreList = meta.getLore();
				 loreList.add(ChatColor.RESET + string);
				 meta.setLore(loreList);
				 }
			 item.setItemMeta(meta);
			 }
		 return item;
		 }

	
	 public synchronized Inventory getPage(int pagenumber) throws InventoryNotFoundException
	 {
		 if (this.INVENTORY.containsKey(Integer.valueOf(pagenumber))) {
			 return (Inventory) this.INVENTORY.get(Integer.valueOf(pagenumber));
			 }
		 throw new InventoryNotFoundException();
		 }

	
	 public int getRow(int slot)
	 {
		 if (slot / 9 <= 0) return 1;
		 if (slot / 9 <= 1) return 2;
		 if (slot / 9 <= 2) return 3;
		 if (slot / 9 <= 3) return 4;
		 if (slot / 9 <= 4) return 5;
		 if (slot / 9 <= 5) return 6;
		 if (slot / 9 <= 6) return 7;
		 if (slot / 9 <= 7) {
			 return 8;
			 }
		 return -1;
		 }

	
	 public int getColumn(int slot)
	 {
		 while (slot > 9) {
			 slot -= 9;
			 }
		 return slot + 1;
		 }

	
	 public int getSlot(int row, int column)
	 {
		 return (row - 1) * 9 + column - 1;
		 }

	
	 public void moveDown(int slot) {
		 if (getRow(slot) + 1 < 7) {
			 int newSlot = getSlot(getRow(slot) + 1, getColumn(slot));
			 ItemStack copy = getCurrentPage().getItem(slot);
			 getCurrentPage().setItem(slot, null);
			 getCurrentPage().setItem(newSlot, copy);
			 }
		 }

	
	 public void moveUp(int slot) {
		 if (getRow(slot) - 1 > 0) {
			 int newSlot = getSlot(getRow(slot) - 1, getColumn(slot));
			 ItemStack copy = getCurrentPage().getItem(slot);
			 getCurrentPage().setItem(slot, null);
			 getCurrentPage().setItem(newSlot, copy);
			 }
		 }

	
	 public void moveLeft(int slot) {
		 if (getColumn(slot) - 1 > 0) {
			 int newSlot = getSlot(getRow(slot), getColumn(slot) - 1);
			 ItemStack copy = getCurrentPage().getItem(slot);
			 getCurrentPage().setItem(slot, null);
			 getCurrentPage().setItem(newSlot, copy);
			 }
		 }

	
	 public void moveRight(int slot) {
		 if (getColumn(slot) + 1 < 10) {
			 int newSlot = getSlot(getRow(slot), getColumn(slot) + 1);
			 ItemStack copy = getCurrentPage().getItem(slot);
			 getCurrentPage().setItem(slot, null);
			 getCurrentPage().setItem(newSlot, copy);
			 }
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.inventory.Menu JD-Core Version: 0.6.2
 */