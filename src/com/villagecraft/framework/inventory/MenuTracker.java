 package com.villagecraft.framework.inventory;


 import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import org.bukkit.inventory.Inventory;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.villagecraft.framework.exceptions.MenuDoesNotExistException;


 public final class MenuTracker
 {
	 private static final Set<Menu> menuSet = new HashSet<>();
	 private static final HashBiMap<Menu, UUID> menuMap = HashBiMap.create();

	
	 public static boolean containsMenu(Menu menu) {
		 if (menuSet.contains(menu))
		 {
			 return true;
			 }
		 return false;
		 }

	
	 public static synchronized Menu getContainingMenu(Inventory inventory)
			 throws MenuDoesNotExistException
	 {
		 Iterator<Menu> menuIterator = menuSet.iterator();
		 while (menuIterator.hasNext()) {
			 Menu next = menuIterator.next();
			 if (next.containsInventory(inventory)) {
				 return next;
				 }
			 }
		
		 throw new MenuDoesNotExistException();
		 }

	
	 public static synchronized UUID getID(Menu menu) {
		 return menuMap.get(menu);
		 }

	
	 public static synchronized boolean containsID(UUID id) {
		 return menuMap.containsValue(id);
		 }

	
	 public static synchronized Menu getMenu(UUID id) {
		 BiMap<UUID,Menu> reverseMap = menuMap.inverse();
		 return reverseMap.get(id);
		 }

	
	 public static synchronized void registerMenu(Menu menu, UUID id) {
		 menuSet.add(menu);
		 menuMap.put(menu, id);
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.inventory.MenuTracker JD-Core Version: 0.6.2
 */