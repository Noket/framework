package com.villagecraft.framework.events.vanilla.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.villagecraft.framework.animation.PlayerJoinSplashAnimation;
import com.villagecraft.framework.events.CustomEventManager;

public class CEML_PlayerJoinEvent implements Listener{

	 @EventHandler
	 public void PlayerJoinEvent(PlayerJoinEvent event)
	 {
		CustomEventManager.callMany(
			new PlayerJoinSplashAnimation(event)
		);
	 }
}

