package com.villagecraft.framework.events.vanilla.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.villagecraft.framework.events.CustomEventManager;
import com.villagecraft.framework.events.custom.implementations.MoonDeathEvent;

public class CEML_PlayerDeathEvent implements Listener{
	 @EventHandler
	 public void PlayerDeathEvent(PlayerDeathEvent e)
	 {
		 if ((e.getDeathMessage().contains("died")) && (e.getEntity().getPlayer().getWorld().getName().equalsIgnoreCase("moon")))
		 {
			 CustomEventManager.call(new MoonDeathEvent(e));
		 }
	 }
}