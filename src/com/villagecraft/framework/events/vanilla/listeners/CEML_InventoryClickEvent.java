package com.villagecraft.framework.events.vanilla.listeners;

import java.util.Objects;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import com.villagecraft.framework.events.CustomEventManager;
import com.villagecraft.framework.events.custom.implementations.MenuCloneStackEvent;
import com.villagecraft.framework.events.custom.implementations.MenuCollectToCursorEvent;
import com.villagecraft.framework.events.custom.implementations.MenuDropAllCursorEvent;
import com.villagecraft.framework.events.custom.implementations.MenuDropAllSlotEvent;
import com.villagecraft.framework.events.custom.implementations.MenuDropOneCursorEvent;
import com.villagecraft.framework.events.custom.implementations.MenuDropOneSlotEvent;
import com.villagecraft.framework.events.custom.implementations.MenuHotbarMoveAndReaddEvent;
import com.villagecraft.framework.events.custom.implementations.MenuHotbarSwapEvent;
import com.villagecraft.framework.events.custom.implementations.MenuMoveToOtherInventoryEvent;
import com.villagecraft.framework.events.custom.implementations.MenuNothingEvent;
import com.villagecraft.framework.events.custom.implementations.MenuPickupAllEvent;
import com.villagecraft.framework.events.custom.implementations.MenuPickupHalfEvent;
import com.villagecraft.framework.events.custom.implementations.MenuPickupOneEvent;
import com.villagecraft.framework.events.custom.implementations.MenuPickupSomeEvent;
import com.villagecraft.framework.events.custom.implementations.MenuPlaceAllEvent;
import com.villagecraft.framework.events.custom.implementations.MenuPlaceOneEvent;
import com.villagecraft.framework.events.custom.implementations.MenuPlaceSomeEvent;
import com.villagecraft.framework.events.custom.implementations.MenuSwapWithCursorEvent;
import com.villagecraft.framework.events.custom.implementations.MenuUnknownEvent;
import com.villagecraft.framework.exceptions.MenuDoesNotExistException;
import com.villagecraft.framework.inventory.Menu;
import com.villagecraft.framework.inventory.MenuTracker;

public class CEML_InventoryClickEvent implements Listener{

 @EventHandler
 private void InventoryClickEvent(InventoryClickEvent event)
 {
	 InventoryAction action = event.getAction();
	 Inventory inventory = event.getInventory();
	
	 if (Objects.isNull(inventory)) {
		 return;
		 }
	
	 try
	 {
		 if (InventoryAction.CLONE_STACK.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuCloneStackEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.COLLECT_TO_CURSOR.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuCollectToCursorEvent(event, menu,
					menu.getMenuID(), event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.DROP_ALL_CURSOR.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuDropAllCursorEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.DROP_ALL_SLOT.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuDropAllSlotEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.DROP_ONE_CURSOR.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuDropOneCursorEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.DROP_ONE_SLOT.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuDropOneSlotEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.HOTBAR_MOVE_AND_READD.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuHotbarMoveAndReaddEvent(event, menu,
					menu.getMenuID(), event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.HOTBAR_SWAP.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuHotbarSwapEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.MOVE_TO_OTHER_INVENTORY.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuMoveToOtherInventoryEvent(event, menu,
					menu.getMenuID(), event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.NOTHING.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuNothingEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.PICKUP_ALL.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuPickupAllEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.PICKUP_HALF.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuPickupHalfEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.PICKUP_ONE.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuPickupOneEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.PICKUP_SOME.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuPickupSomeEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.PLACE_ALL.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuPlaceAllEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.PLACE_ONE.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuPlaceOneEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.PLACE_SOME.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuPlaceSomeEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.SWAP_WITH_CURSOR.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuSwapWithCursorEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		
		 if (InventoryAction.UNKNOWN.equals(action)) {
			 Menu menu = MenuTracker.getContainingMenu(inventory);
			 CustomEventManager.call(new MenuUnknownEvent(event, menu, menu.getMenuID(),
					event.getRawSlot(), event.getCurrentItem()));
			 }
		 }
	 catch (MenuDoesNotExistException e)
	 {
		 }
	 }
}
