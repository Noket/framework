package com.villagecraft.framework.events.vanilla.listeners;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import com.villagecraft.framework.events.CustomEventManager;
import com.villagecraft.framework.events.custom.implementations.PlayerCommandEvent;

public class CEML_PlayerCommandPreprocessEvent implements Listener{
	 @EventHandler
	 private void PlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event)
	 {
		 String message = event.getMessage();
		
		 String[] splitMessage = message.split(" ");
		
		 String possibleCommand = splitMessage[0];
		
		 possibleCommand = possibleCommand.substring(1);
		
		 CommandSender sender = event.getPlayer();
		 String[] args;
		 if (splitMessage.length > 1) {
			 args = (String[]) Arrays.copyOfRange(splitMessage, 1, splitMessage.length);
			 }
		 else {
			 args = new String[0];
			 }
		
		 Command command = Bukkit.getServer().getPluginCommand(possibleCommand);
		
		 CustomEventManager.call(new PlayerCommandEvent(event, sender, command, possibleCommand, args));
		 }
}
