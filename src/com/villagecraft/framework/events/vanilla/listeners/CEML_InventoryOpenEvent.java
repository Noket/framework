package com.villagecraft.framework.events.vanilla.listeners;

import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;

import com.villagecraft.framework.events.CustomEventManager;
import com.villagecraft.framework.events.custom.implementations.MenuOpenEvent;
import com.villagecraft.framework.exceptions.MenuDoesNotExistException;
import com.villagecraft.framework.inventory.Menu;
import com.villagecraft.framework.inventory.MenuTracker;

public class CEML_InventoryOpenEvent implements Listener{
	 @EventHandler
	 private void InventoryOpenEvent(InventoryOpenEvent event)
	 {
		 try
		 {
			 Menu menu = MenuTracker.getContainingMenu(event.getInventory());
			 if (Objects.nonNull(menu)) CustomEventManager.call(new MenuOpenEvent(event));
			 }
		 catch (MenuDoesNotExistException e)
		 {
			 }
		 }
}
