package com.villagecraft.framework.events.custom.implementations;

import org.bukkit.event.inventory.InventoryOpenEvent;

import com.villagecraft.framework.events.custom.CustomEventObservable;

public class MenuOpenEvent extends CustomEventObservable {
	InventoryOpenEvent baseEvent;

	public MenuOpenEvent(InventoryOpenEvent event) {
		this.baseEvent = event;
	}

	public InventoryOpenEvent getUnderlyingEvent() {
		return this.baseEvent;
	}
}

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.events.custom.MenuOpenEvent JD-Core Version: 0.6.2
 */