package com.villagecraft.framework.events.custom.implementations;

import org.bukkit.event.entity.PlayerDeathEvent;

import com.villagecraft.framework.events.custom.CustomEventObservable;
import com.villagecraft.framework.toolset.Numbers;

public class MoonDeathEvent extends CustomEventObservable {

	public MoonDeathEvent(PlayerDeathEvent event) {
		String name = event.getEntity().getName();
	
		switch(Numbers.generateRandom(1, 6)) {
			case 1: event.setDeathMessage(name + " suffocated on the moon."); break;
			case 2: event.setDeathMessage(name + " tried to breathe in space."); break;
			case 3: event.setDeathMessage(name + " attempted to eat moon cheese."); break;
			case 4: event.setDeathMessage(name + " came to the moon unprepared."); break;
			case 5: event.setDeathMessage(name + " forgot their oxygen supply."); break;
			case 6: event.setDeathMessage(name + " lost their mind in the depths of space."); break;
	}
		
	/*
	 }if(e.getDeathMessage().contains("Moon Whisp"))

	{
		String name = event.getEntity().getName();

		byte r = (byte) Number.generateRandom(1, 5);
		if (r == 1)
			event.setDeathMessage(name + " got too close to a Moon Whisp.");
		if (r == 2)
			event.setDeathMessage(name + " was blown to bits by a Moon Whisp.");
		if (r == 3)
			event.setDeathMessage(name + " found out that Moon Whisps aren't nice.");
		if (r == 4)
			event.setDeathMessage(name + " trusted a Moon Whisp too much.");
		if (r == 5) {
			event.setDeathMessage(name + " tried to make friends with a Moon Whisp.");
		}
	}if(e.getDeathMessage().contains("Ethereal Being"))
	{
		String name = event.getEntity().getName();

		byte r = (byte) Number.generateRandom(1, 3);
		if (r == 1)
			event.setDeathMessage(name + "'s life force was stolen by an Ethereal Being.");
		if (r == 2)
			event.setDeathMessage("Spaceport Imbrium lost contact with " + name
					+ ". Their last transmission was filled with screaming.");
		if (r == 3) {
			Integer rand = Integer.valueOf(Number.generateRandom(30, 70));
			char[] num = String.valueOf(rand).toCharArray();
			String str1;
			if (num[1] == '1')
				str1 = "st";
			if (num[1] == '2')
				str1 = "nd";
			String suffix;
			if (num[1] == '3')
				suffix = "rd";
			else {
				suffix = "th";
			}
			e.setDeathMessage(name + " was possessed by an Ethereal Being. " + name + " was the " + rand + suffix
					+ " person to be lost this month.");
		}
	}*/
	}
}
