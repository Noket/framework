package com.villagecraft.framework.events.custom.implementations;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import com.villagecraft.framework.events.custom.CustomEventObservable;

public class PlayerCommandEvent extends CustomEventObservable {
	private CommandSender sender;
	private Command command;
	private String[] args;
	private PlayerCommandPreprocessEvent event;
	private String commandText;

	public PlayerCommandEvent(PlayerCommandPreprocessEvent event, CommandSender sender, Command command,
			String possibleCommand, String[] args) {
		this.sender = sender;
		this.command = command;
		this.args = args;
		this.event = event;
		this.commandText = possibleCommand;
	}

	public CommandSender getCommandSender() {
		return this.sender;
	}

	public Command getCommand() {
		return this.command;
	}

	public String[] getArguments() {
		return this.args;
	}

	public PlayerCommandPreprocessEvent getPreprocessEvent() {
		return this.event;
	}

	public String getCommandString() {
		return this.commandText;
	}
}

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.events.custom.PlayerCommandEvent JD-Core Version:
 * 0.6.2
 */