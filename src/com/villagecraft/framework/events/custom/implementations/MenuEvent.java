package com.villagecraft.framework.events.custom.implementations;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.villagecraft.framework.events.custom.CustomEventObservable;
import com.villagecraft.framework.inventory.Menu;

public abstract class MenuEvent extends CustomEventObservable {
	
	private InventoryClickEvent encapsulatedEvent;
	private Player eventPlayer;
	private Inventory eventInventory;
	private Menu eventMenu;
	private UUID eventMenuID;
	private int eventSlot;
	private ItemStack eventItem;
	private ItemMeta eventMeta;
	private boolean hasMeta;
	private List<String> eventLore;
	private boolean hasLore;
	private String eventDisplayName;
	private boolean hasDisplayName;
	private Map<Enchantment, Integer> eventEnchantments;
	private boolean hasEnchantments;

	public MenuEvent(InventoryClickEvent event, Menu menu, UUID menuID, int slot, ItemStack item) {
		this.encapsulatedEvent = event;
		this.eventMenu = menu;
		this.eventInventory = menu.getCurrentPage();
		this.eventMenuID = menuID;
		this.eventSlot = slot;
		this.eventItem = item;
		this.eventPlayer = ((Player) event.getWhoClicked());

		if (this.eventItem.hasItemMeta()) {
			this.eventMeta = this.eventItem.getItemMeta();
			this.hasMeta = true;
			if (this.eventMeta.hasLore()) {
				this.eventLore = this.eventMeta.getLore();
				this.hasLore = true;
			}
			if (this.eventMeta.hasDisplayName()) {
				this.eventDisplayName = this.eventMeta.getDisplayName();
				this.hasDisplayName = true;
			}
			if (this.eventMeta.hasEnchants()) {
				this.eventEnchantments = this.eventMeta.getEnchants();
				this.hasEnchantments = true;
			}
		}
	}

	public boolean itemHasLore() {
		return this.hasLore;
	}

	public boolean itemHasMeta() {
		return this.hasMeta;
	}

	public boolean itemHasDisplayName() {
		return this.hasDisplayName;
	}

	public boolean itemHasEnchants() {
		return this.hasEnchantments;
	}

	public List<String> getItemLore() {
		return this.eventLore;
	}

	public String getItemDisplayName() {
		return this.eventDisplayName;
	}

	public Map<Enchantment, Integer> getItemEnchantments() {
		return this.eventEnchantments;
	}

	public Player getPlayer() {
		return this.eventPlayer;
	}

	public int getSlot() {
		return this.eventSlot;
	}

	public Menu getMenu() {
		return this.eventMenu;
	}

	public UUID getMenuID() {
		return this.eventMenuID;
	}

	public Inventory getClickedMenuPage() {
		return this.eventInventory;
	}

	public InventoryClickEvent getPlayerClickEvent() {
		return this.encapsulatedEvent;
	}
}

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.events.custom.MenuEvent JD-Core Version: 0.6.2
 */