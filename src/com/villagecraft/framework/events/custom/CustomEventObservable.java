 package com.villagecraft.framework.events.custom;


 import java.util.ArrayList;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import com.google.common.collect.Lists;

/**
 * Extend this class to begin using custom events
 * @author jim
 *
 */
 public abstract class CustomEventObservable extends Event implements Listener
 {
	 private static HandlerList handlers = new HandlerList();
	 
	 public HandlerList getHandlers()
	 {
		 return handlers;
		 }

	
	 public static HandlerList getHandlerList() {
		 return handlers;
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.events.custom.CustomEventBase JD-Core Version:
 * 0.6.2
 */