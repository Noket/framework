package com.villagecraft.framework.events;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.PluginManager;

import com.villagecraft.framework.Framework;
import com.villagecraft.framework.PluginBranch;
import com.villagecraft.framework.events.custom.CustomEventObservable;
import com.villagecraft.framework.events.vanilla.listeners.CEML_InventoryClickEvent;
import com.villagecraft.framework.events.vanilla.listeners.CEML_InventoryOpenEvent;
import com.villagecraft.framework.events.vanilla.listeners.CEML_PlayerCommandPreprocessEvent;
import com.villagecraft.framework.events.vanilla.listeners.CEML_PlayerDeathEvent;
import com.villagecraft.framework.events.vanilla.listeners.CEML_PlayerJoinEvent;

 public class CustomEventManager implements Listener
 {
	private static CustomEventManager CEM_SINGLETON = new CustomEventManager();
	private static final HashMap<Class<? extends PluginBranch>, PluginBranch> constructedObjectMap = new HashMap<>();
	private static final List<CustomEventObservable> eventClasses = new ArrayList<>();
	
	/**
	 * This is called "onEnable", registering listeners to Bukkit
	 */
	public static void registerListeners(){
		Bukkit.getPluginManager().registerEvents(new CEML_InventoryClickEvent(), Framework.getSingleton());
		Bukkit.getPluginManager().registerEvents(new CEML_InventoryOpenEvent(), Framework.getSingleton());
		Bukkit.getPluginManager().registerEvents(new CEML_PlayerCommandPreprocessEvent(), Framework.getSingleton());
		Bukkit.getPluginManager().registerEvents(new CEML_PlayerDeathEvent(), Framework.getSingleton());
		Bukkit.getPluginManager().registerEvents(new CEML_PlayerJoinEvent(), Framework.getSingleton());

	}

	 public static CustomEventManager getSingleton() {
		 return CEM_SINGLETON;
	 }
	 
	 public static void registerEventClass(CustomEventObservable event) {
		 eventClasses.add(event);
	 }
	 protected List<CustomEventObservable> getRegisteredEvents(){
		 return eventClasses;
	 }
	 
	 public static void call(CustomEventObservable obj) {
		 Bukkit.getPluginManager().callEvent(obj);
	 }
	 
	 public static void callMany(CustomEventObservable ... obj) {
		 List<CustomEventObservable> eventSet = Arrays.asList(obj);
		 Iterator<CustomEventObservable> eventIter = eventSet.iterator();
		 
		 PluginManager manager = Bukkit.getPluginManager();
		 
		 while(eventIter.hasNext()){
			 manager.callEvent(eventIter.next());
		 }
		 
	 }
 }	
/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.events.CustomEventManager JD-Core Version: 0.6.2
 */