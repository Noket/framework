package com.villagecraft.framework.toolset;

public class ChatColours {
	public static final String DarkRed = "§4";
	public static final String LightRed = "§c";
	public static final String Orange = "§6";
	public static final String Yellow = "§e";
	public static final String DarkGreen = "§2";
	public static final String LightGreen = "§a";
	public static final String LightAqua = "§b";
	public static final String DarkAqua = "§3";
	public static final String DarkBlue = "§1";
	public static final String LightBlue = "§9";
	public static final String LightPurple = "§d";
	public static final String DarkPurple = "§5";
	public static final String White = "§f";
	public static final String LightGrey = "§7";
	public static final String DarkGrey = "§8";
	public static final String Black = "§0";
	public static final String Bold = "§l";
	public static final String Underline = "§n";
	public static final String Itallic = "§o";
	public static final String Magic = "§k";
	public static final String Strike = "§m";
	public static final String Reset = "§r";
	public static final String NewLine = "\n";

	public class Generic {
		public static final String pvpisle = "§a";
		public static final String pvpisle_ = "§2";
		public static final String thelake = "§d";
		public static final String thelake_ = "§5";
		public static final String fakeop = "§9";
		public static final String fakeop_ = "§1";
		public static final String themoon = "§c";
		public static final String themoon_ = "§4";

		public Generic() {
		}
	}
}

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.ChatColours JD-Core Version: 0.6.2
 */