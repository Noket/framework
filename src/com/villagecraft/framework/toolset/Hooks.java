package com.villagecraft.framework.toolset;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.PluginManager;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldguard.WorldGuard;

public final class Hooks {

	public static class hWorldGuard{
		public static WorldGuard instance() {
			return 	WorldGuard.getInstance();
		}
	}
	
	public static class hProtocolLib{
		public static ProtocolManager instance() {
			return ProtocolLibrary.getProtocolManager();
		}
	}
	
	public static class hWorldEdit{
		public static WorldEdit instance() {
			return WorldEdit.getInstance();
		}
	}
	
	public static class hBukkit{
		public static Server server() {
			return Bukkit.getServer();
		}
		
		public static PluginManager pluginManager() {
			return Bukkit.getPluginManager();
		}
		
		public static ConsoleCommandSender commandsender() {
			return Bukkit.getConsoleSender();
		}

	}
	


}
