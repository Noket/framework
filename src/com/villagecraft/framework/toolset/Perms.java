package com.villagecraft.framework.toolset;

public class Perms {
	public class misc {
		public static final String fakeOp = "vcmod.fakeop";
		public static final String fakeplayer = "vcmod.fakeplayer";

		public misc() {
		}
	}

	public class pvpIsle {
		public static final String spawns = "vcmod.pvpisle.setspawns";
		public static final String start = "vcmod.pvpisle.canstart";
		public static final String join = "vcmod.pvpisle.canjoin";
		public static final String leave = "vcmod.pvpisle.canleave";
		public static final String spectate = "vcmod.pvpisle.canspectate";
		public static final String set = "vcmod.pvpisle.setpoint";
		public static final String chatOverride = "vcmod.pvpisle.chatoutofgame";

		public pvpIsle() {
		}
	}

	public class theLake {
		public theLake() {
		}
	}
}

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.Perms JD-Core Version: 0.6.2
 */