 package com.villagecraft.framework.toolset.nbt.tags;


 import org.bukkit.entity.LivingEntity;

import com.villagecraft.framework.toolset.nbt.NBT;
import com.villagecraft.framework.toolset.nbt.Tag;

import net.minecraft.server.v1_12_R1.NBTTagCompound;


 public class TagId implements Tag
 {
	 private String id;

	
	 public TagId(String id)
	 {
		 this.id = id;
		 }

	
	 public void applyData(LivingEntity entity)
	 {
		 NBTTagCompound tag = NBT.copyTagFromEntity(entity, null);
		 tag.setString("id", this.id);
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.nbt.tags.TagId JD-Core Version: 0.6.2
 */