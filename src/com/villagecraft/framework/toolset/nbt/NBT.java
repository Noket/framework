 package com.villagecraft.framework.toolset.nbt;


 import org.bukkit.craftbukkit.v1_12_R1.entity.CraftLivingEntity;
import org.bukkit.entity.LivingEntity;

import net.minecraft.server.v1_12_R1.EntityLiving;
import net.minecraft.server.v1_12_R1.NBTTagCompound;


 public final class NBT
 {
	 public static NBTTagCompound copyTagFromEntity(LivingEntity entity, NBTTagCompound compound)
	 {
		 EntityLiving nmsentity = ((CraftLivingEntity) entity).getHandle();
		
		 if (compound == null) {
			 compound = new NBTTagCompound();
			 nmsentity.b(compound);
			 } else {
			 nmsentity.b(compound);
			 }
		 return compound;
		 }

	
	 public static void applyTagToEntity(NBTTagCompound tag, LivingEntity entity)
	 {
		 if (tag != null) {
			 EntityLiving nmsentity = ((CraftLivingEntity) entity).getHandle();
			 nmsentity.a(tag);
			 }
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.nbt.NBT JD-Core Version: 0.6.2
 */