 package com.villagecraft.framework.toolset.core;


 import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.EntityEffect;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;


 public abstract class EntityWrapper implements Entity
 {
	 private Entity delegator;

	
	 public EntityWrapper(Entity e)
	 {
		 this.delegator = e;
		 }

	
	 public abstract boolean getHasParent();

	
	 public abstract void setHasParent(boolean paramBoolean);

	
	 public final void setMetadata(String metadataKey, MetadataValue newMetadataValue)
	 {
		 this.delegator.setMetadata(metadataKey, newMetadataValue);
		 }

	
	 public final List<MetadataValue> getMetadata(String metadataKey)
	 {
		 return this.delegator.getMetadata(metadataKey);
		 }

	
	 public final boolean hasMetadata(String metadataKey)
	 {
		 return this.delegator.hasMetadata(metadataKey);
		 }

	
	 public final void removeMetadata(String metadataKey, Plugin owningPlugin)
	 {
		 this.delegator.removeMetadata(metadataKey, owningPlugin);
		 }

	
	 public final void sendMessage(String message)
	 {
		 this.delegator.sendMessage(message);
		 }

	
	 public final void sendMessage(String[] messages)
	 {
		 this.delegator.sendMessage(messages);
		 }

	
	 public final String getName()
	 {
		 return this.delegator.getName();
		 }

	
	 public final boolean isPermissionSet(String name)
	 {
		 return this.delegator.isPermissionSet(name);
		 }

	
	 public final boolean isPermissionSet(Permission perm)
	 {
		 return this.delegator.isPermissionSet(perm);
		 }

	
	 public final boolean hasPermission(String name)
	 {
		 return this.delegator.hasPermission(name);
		 }

	
	 public final boolean hasPermission(Permission perm)
	 {
		 return this.delegator.hasPermission(perm);
		 }

	
	 public final PermissionAttachment addAttachment(Plugin plugin, String name, boolean value)
	 {
		 return this.delegator.addAttachment(plugin, name, value);
		 }

	
	 public final PermissionAttachment addAttachment(Plugin plugin)
	 {
		 return this.delegator.addAttachment(plugin);
		 }

	
	 public final PermissionAttachment addAttachment(Plugin plugin, String name, boolean value, int ticks)
	 {
		 return this.delegator.addAttachment(plugin, name, value, ticks);
		 }

	
	 public final PermissionAttachment addAttachment(Plugin plugin, int ticks)
	 {
		 return this.delegator.addAttachment(plugin, ticks);
		 }

	
	 public final void removeAttachment(PermissionAttachment attachment)
	 {
		 this.delegator.removeAttachment(attachment);
		 }

	
	 public final void recalculatePermissions()
	 {
		 this.delegator.recalculatePermissions();
		 }

	
	 public final Set<PermissionAttachmentInfo> getEffectivePermissions()
	 {
		 return this.delegator.getEffectivePermissions();
		 }

	
	 public final boolean isOp()
	 {
		 return this.delegator.isOp();
		 }

	
	 public final void setOp(boolean value)
	 {
		 this.delegator.setOp(value);
		 }

	
	 public final boolean eject()
	 {
		 return this.delegator.eject();
		 }

	
	 public final String getCustomName()
	 {
		 return this.delegator.getCustomName();
		 }

	
	 public final Location getLocation()
	 {
		 return this.delegator.getLocation();
		 }

	
	 public final Location getLocation(Location loc)
	 {
		 return this.delegator.getLocation(loc);
		 }

	
	 public final Vector getVelocity()
	 {
		 return this.delegator.getVelocity();
		 }

	
	 public final boolean isOnGround()
	 {
		 return this.delegator.isOnGround();
		 }

	
	 public final World getWorld()
	 {
		 return this.delegator.getWorld();
		 }

	
	 public final List<Entity> getNearbyEntities(double x, double y, double z)
	 {
		 return this.delegator.getNearbyEntities(x, y, z);
		 }

	
	 public final int getEntityId()
	 {
		 return this.delegator.getEntityId();
		 }

	
	 public final int getFireTicks()
	 {
		 return this.delegator.getFireTicks();
		 }

	
	 public final int getMaxFireTicks()
	 {
		 return this.delegator.getMaxFireTicks();
		 }

	
	 public final void remove()
	 {
		 this.delegator.remove();
		 }

	
	 public final boolean isDead()
	 {
		 return this.delegator.isDead();
		 }

	
	 public final boolean isValid()
	 {
		 return this.delegator.isValid();
		 }

	
	 public final Server getServer()
	 {
		 return this.delegator.getServer();
		 }

	
	 public final Entity getPassenger()
	 {
		 return this.delegator.getPassenger();
		 }

	
	 public final boolean isEmpty()
	 {
		 return this.delegator.isEmpty();
		 }

	
	 public final float getFallDistance()
	 {
		 return this.delegator.getFallDistance();
		 }

	
	 public final void setFallDistance(float distance)
	 {
		 this.delegator.setFallDistance(distance);
		 }

	
	 public final EntityDamageEvent getLastDamageCause()
	 {
		 return this.delegator.getLastDamageCause();
		 }

	
	 public final UUID getUniqueId()
	 {
		 return this.delegator.getUniqueId();
		 }

	
	 public final int getTicksLived()
	 {
		 return this.delegator.getTicksLived();
		 }

	
	 public final void playEffect(EntityEffect type)
	 {
		 this.delegator.playEffect(type);
		 }

	
	 public final EntityType getType()
	 {
		 return this.delegator.getType();
		 }

	
	 public final boolean isInsideVehicle()
	 {
		 return this.delegator.isInsideVehicle();
		 }

	
	 public final Entity getVehicle()
	 {
		 return this.delegator.getVehicle();
		 }

	
	 public final boolean isCustomNameVisible()
	 {
		 return this.delegator.isCustomNameVisible();
		 }

	
	 public final boolean leaveVehicle()
	 {
		 return this.delegator.leaveVehicle();
		 }

	
	 public final void setCustomName(String name)
	 {
		 this.delegator.setCustomName(name);
		 }

	
	 public final void setCustomNameVisible(boolean flag)
	 {
		 this.delegator.setCustomNameVisible(flag);
		 }

	
	 public final void setVelocity(Vector velocity)
	 {
		 this.delegator.setVelocity(velocity);
		 }

	
	 public final boolean teleport(Location location)
	 {
		 return this.delegator.teleport(location);
		 }

	
	 public final boolean teleport(Location location, PlayerTeleportEvent.TeleportCause cause)
	 {
		 return this.delegator.teleport(location, cause);
		 }

	
	 public final boolean teleport(Entity destination)
	 {
		 return this.delegator.teleport(destination);
		 }

	
	 public final boolean teleport(Entity destination, PlayerTeleportEvent.TeleportCause cause)
	 {
		 return this.delegator.teleport(destination, cause);
		 }

	
	 public final void setFireTicks(int ticks)
	 {
		 this.delegator.setFireTicks(ticks);
		 }

	
	 public final boolean setPassenger(Entity passenger)
	 {
		 return this.delegator.setPassenger(passenger);
		 }

	
	 public final void setLastDamageCause(EntityDamageEvent event)
	 {
		 this.delegator.setLastDamageCause(event);
		 }

	
	 public final void setTicksLived(int value)
	 {
		 this.delegator.setTicksLived(value);
		 }

	
	 public final Entity.Spigot spigot()
	 {
		 return this.delegator.spigot();
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.core.EntityWrapper JD-Core Version: 0.6.2
 */