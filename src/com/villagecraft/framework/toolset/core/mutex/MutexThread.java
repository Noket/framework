 package com.villagecraft.framework.toolset.core.mutex;


 import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;
import java.util.concurrent.ThreadFactory;

import org.apache.commons.lang.Validate;


 public final class MutexThread extends Thread implements ThreadFactory
 {
	 private Thread mutexThread;
	 private String threadName;
	 private String threadUUID;

	
	 protected MutexThread(Runnable runnable)
	 {
		 newThread(runnable);
		 }

	
	 protected MutexThread(Class<? extends Runnable> clazz)
	 {
		 Class runnableClass = clazz;
		 Constructor runnableConstructor = null;
		 Runnable runnable = null;
		 try
		 {
			 Validate.notNull(runnableClass);
			 runnableConstructor = runnableClass.getConstructor(new Class[0]);
			 }
		 catch (NoSuchMethodException | SecurityException localNoSuchMethodException)
		 {
			 }
		 try {
			 Validate.notNull(runnableConstructor);
			 runnable = (Runnable) runnableConstructor.newInstance(new Object[0]);
			 }
		 catch (InstantiationException | IllegalAccessException
				| InvocationTargetException localInstantiationException)
		 {
			 }
		 try {
			 Validate.notNull(runnable);
			 newThread(runnable);
			 }
		 catch (IllegalArgumentException localIllegalArgumentException) {
			 }
		 newThread(runnable);
		 }

	
	 public Thread unwrapThread()
	 {
		 if (this.mutexThread == null)
		 {
			 Thread dud = new Thread();
			 dud.setName("DUD");
			 return dud;
			 }
		
		 return this.mutexThread;
		 }

	
	 public Thread newThread(Runnable runnable)
	 {
		 this.mutexThread = new Thread(runnable);
		 this.mutexThread.setName(generateThreadName(runnable));
		 return this.mutexThread;
		 }

	
	 public String generateThreadName(Runnable runnable)
	 {
		 StringBuilder builder = new StringBuilder();
		 this.threadName = runnable.getClass().getSimpleName();
		 this.threadUUID = UUID.randomUUID().toString();
		 builder.append(this.threadName).append("#").append(this.threadUUID);
		
		 String building = builder.toString();
		
		 return building;
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.core.mutex.MutexThread JD-Core Version:
 * 0.6.2
 */