 package com.villagecraft.framework.toolset.core.mutex;


 import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.inventory.Inventory;
import org.bukkit.material.MaterialData;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.permissions.Permissible;


 public final class MutexThreadManager
 {
	 private static ConcurrentLinkedQueue<Map<MutexThread, Class<?>>> queue;
	 private static ConcurrentHashMap<Class<? extends BlockState>, ConcurrentHashMap<UUID, CopyOnWriteArrayList<? extends BlockState>>> blockResource = new ConcurrentHashMap();
	 private static ConcurrentHashMap<Class<? extends Entity>, ConcurrentHashMap<UUID, CopyOnWriteArrayList<? extends Entity>>> entityResource = new ConcurrentHashMap();
	 private static ConcurrentHashMap<Class<? extends Event>, ConcurrentHashMap<UUID, CopyOnWriteArrayList<? extends BlockState>>> eventResource = new ConcurrentHashMap();
	 private static ConcurrentHashMap<Class<? extends Inventory>, ConcurrentHashMap<UUID, CopyOnWriteArrayList<? extends Inventory>>> inventoryResource = new ConcurrentHashMap();
	 private static ConcurrentHashMap<Class<? extends MaterialData>, ConcurrentHashMap<UUID, CopyOnWriteArrayList<? extends MaterialData>>> materialResource = new ConcurrentHashMap();
	 private static ConcurrentHashMap<Class<? extends MetadataValue>, ConcurrentHashMap<UUID, CopyOnWriteArrayList<? extends MetadataValue>>> metadataResource = new ConcurrentHashMap();
	 private static ConcurrentHashMap<Class<? extends Permissible>, ConcurrentHashMap<UUID, CopyOnWriteArrayList<? extends Permissible>>> permissibleResource = new ConcurrentHashMap();

	
	 public MutexThreadManager()
	 {
		 queue = new ConcurrentLinkedQueue();
		 }

	
	 public static void requestResourceAccess(MutexThread thread, Class<?> clazz)
	 {
		 Map request = new HashMap();
		 request.put(thread, clazz);
		 queue.add(request);
		
		 Server server = Bukkit.getServer();
		 }

	
	 protected static enum MutexResources
	 {
		 BLOCK,  ENTITY,  EVENT,  INVENTORY,  MATERIAL,  METADATA,  PERMISSIBLE;
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.core.mutex.MutexThreadManager JD-Core
 * Version: 0.6.2
 */