 package com.villagecraft.framework.toolset.core.factory;


 import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.plugin.PluginManager;


 public class WrappedEvent
 {
	 private Event event;
	 private static PluginManager pluginManager = Bukkit.getPluginManager();

	
	 public WrappedEvent(Event event)
	 {
		 this.event = event;
		 }

	
	 public boolean call()
	 {
		 try
		 {
			 pluginManager.callEvent(this.event);
			 return true;
			 }
		 catch (IllegalStateException exception) {
			 }
		 return false;
		 }

	
	 public Event getEvent()
	 {
		 return this.event;
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.core.factory.WrappedEvent JD-Core Version:
 * 0.6.2
 */