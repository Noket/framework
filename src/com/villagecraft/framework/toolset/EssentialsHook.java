 package com.villagecraft.framework.toolset;


 import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import com.earth2me.essentials.Essentials;
import com.villagecraft.framework.exceptions.FailedHookException;


 public final class EssentialsHook
 {
	 private static Essentials essentials = null;

	
	 public static Essentials getEssentialsInstance() throws FailedHookException
	 {
		 if (Objects.isNull(essentials))
		 {
			 Plugin plugin = Bukkit.getPluginManager().getPlugin("Essentials");
			
			 if (((plugin instanceof Essentials)) && (Objects.nonNull(plugin)))
			 {
				 essentials = (Essentials) plugin;
				 return essentials;
				 }
			
			 throw new FailedHookException();
			 }
		
		 return essentials;
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.EssentialsHook JD-Core Version: 0.6.2
 */