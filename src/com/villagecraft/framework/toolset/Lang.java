package com.villagecraft.framework.toolset;

public class Lang {
	public class misc {
		public static final String argsError = "§cError! Too many arguments. ";
		public static final String fakeOp = "§eYou are now OP!";
		public static final String permError = "§cError! Insufficient permissions.";
		public static final String setSpawn = "§aSet location to ";

		public misc() {
		}
	}

	public class pvpIsle {
		public static final String pirates = "§5Pirates";
		public static final String islanders = "§6Islanders";
		public static final String help = "§a§nPVP Isle Help!§r§a\n§2/pvpisle join§a - Joins the game\n§2/pvpisle spec§a - Spectate the current game\n§2/pvpisle start§a - Begin the game you are in\n§2/pvpisle leave§a - Leaves the game\n§2/pvpisle info§a - View some info about the game\n";
		public static final String info = "§aPVP Isle is a PVP game made for VillageCraft, which allows usersto play a competitive game together, with little set-up. There are two teams to play on, §5Pirates§a and §6Islanders§a. You must gain control over varius capture points around the map,while trying to stay away from the opposing team. It was originally built using command blocks and the scoreboard API, but has sincebecome a custom plugin made just for VC by luisc99.";
		public static final String setSpawn = "§aTo set a spawn point, please define one of the following:§n- Lobby\n- Pirate\n- Island\n- Spec";
		public static final String teamspeak = "§aYou may wish to chat with people playing the game. If so, you may want to consider using our teamspeak!The IP adress is §2ts2.gameservers.com:9106§a and you can join the channel labled PVPIsle! Thers is seperate sub channelf for the different teams too, or you can chat as one!For more infomation, you can use /voice";
		public static final String joinLobby = "§aYou have entered the lobby!";
		public static final String joinSpectate = "§aYou are now spectating the game!";
		public static final String leave = "§aYou have left the game!";
		public static final String cannotStart = "§9Error! Cannot start!";

		public pvpIsle() {
		}
	}

	public class theLake {
		public static final String help = "";

		public theLake() {
		}
	}
}

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.Lang JD-Core Version: 0.6.2
 */