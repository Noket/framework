 package com.villagecraft.framework.toolset;


 import java.util.UUID;

import org.bukkit.ChunkSnapshot;

import com.villagecraft.framework.exceptions.UncheckedSecurityException;


 public final class ObjectFactory
 {
	 private static final ObjectFactory SINGLETON = new ObjectFactory();

	
	 private ObjectFactory() throws UncheckedSecurityException
	 {
		 if (SINGLETON == null) throw new UncheckedSecurityException();
		 }

	
	 public static ObjectFactory getFactory()
	 {
		 return SINGLETON;
		 }

	
	 public ChunkLocationWrapper newChunkLocationWrapper(ChunkSnapshot ch)
	 {
		 ChunkLocationWrapper wrapper = new ChunkLocationWrapper(ch);
		 return wrapper;
		 }

	
	 protected static final class FactoryKey
	 {
		 private static final FactoryKey KEY = new FactoryKey(ObjectFactory.SINGLETON);
		 private static UUID id = UUID.randomUUID();
		 private static boolean keyCreated = false;

		
		 private FactoryKey(ObjectFactory factory) throws UncheckedSecurityException {
			 if (KEY == null) {
				 throw new UncheckedSecurityException();
				 }
			
			 if (KEY != null) keyCreated = true;
			 }

		
		 protected UUID getID()
		 {
			 return id;
			 }

		
		 protected FactoryKey getKey() {
			 return KEY;
			 }
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.ObjectFactory JD-Core Version: 0.6.2
 */