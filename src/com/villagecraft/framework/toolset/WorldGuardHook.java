 package com.villagecraft.framework.toolset;


 import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.compress.utils.Lists;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;


 public class WorldGuardHook
 {
	 
	 private HashMap<World, RegionManager> regionManagerMap = new HashMap<World,RegionManager>();
	 protected WorldGuardPlugin wgplugin;
	 protected WorldEditPlugin weplugin;
	 protected WorldEdit worldedit;
	
	 public WorldGuardHook()
	 {
		 
		 Plugin plugin = Bukkit.getPluginManager().getPlugin("WorldGuard");
		 if (plugin != null)
		 {
			 if ((plugin instanceof WorldGuardPlugin))
			 {
				 this.wgplugin = ((WorldGuardPlugin) plugin);
				 }
			
			 }
		 
		 try {
			weplugin = wgplugin.getWorldEdit();
		} catch (CommandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 worldedit = weplugin.getWorldEdit();
		 
		 
		 Iterator<World> worldIterator = Bukkit.getWorlds().iterator();
		 while (worldIterator.hasNext())
		 {
			 World temp = (World) worldIterator.next();
			 
//			 this.regionManagerMap.put(temp, wgplugin.getWorl);
			 }
		 }

	
	 public HashMap<World, RegionManager> getAllRegionManagers()
	 {
		 return this.regionManagerMap;
		 }

	
	 public WorldGuardPlugin getWGPlugin()
	 {
		 return this.wgplugin;
		 }

	
	 public List<String> getApplicableRegions(Player player)
	 {
		 WorldGuardHook hook = new WorldGuardHook();
		 HashMap<World,RegionManager> regionManagers = hook.getAllRegionManagers();
		 RegionManager regionManager = (RegionManager) regionManagers.get(player.getWorld());
		
		 
//		ApplicableRegionSet regionSet = regionManager.getApplicableRegions(player.getLocation());
		
//		 Iterator<ProtectedRegion> regionSetIterator = regionSet.iterator();
		 ArrayList<String> returnString = new ArrayList<String>();
		
//		 while (regionSetIterator.hasNext())
//		 {
//			 returnString.add(((ProtectedRegion) regionSetIterator.next()).getId());
//			 }
//		
//		 if (returnString.equals(null)) {
//			 return Collections.emptyList();
//			 }
//		
//		 return returnString;
		 return Lists.newArrayList();
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.WorldGuardHook JD-Core Version: 0.6.2
 */