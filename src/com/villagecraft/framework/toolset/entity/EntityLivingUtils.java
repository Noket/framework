package com.villagecraft.framework.toolset.entity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.comphenix.protocol.wrappers.BlockPosition;

import net.minecraft.server.v1_12_R1.EntityLiving;
import net.minecraft.server.v1_12_R1.IBlockData;

/**
 * Implements facade pattern wherever possible, and renames other poorly named methods elsewhere. The goal is readability.
 * @author james
 *
 */
public class EntityLivingUtils {
	
	public static void damageOutOfWorld(EntityLiving entity){
		//entity.Q();
	}
	
	public static void registerDatawatchers(EntityLiving entity){
		try {
			Method i = entity.getClass().getDeclaredMethod("i");
			i.setAccessible(true);
			i.invoke(entity);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void dosomething(EntityLiving entity, double d0, boolean flag, IBlockData iblockdata, BlockPosition blockposition){
		
	}
}
