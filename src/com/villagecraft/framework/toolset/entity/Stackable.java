package com.villagecraft.vcplugin.toolset.modules.interfaces.entity;

import org.bukkit.entity.Entity;

public interface Stackable {
	
	/**
	 * Method for seeing if the entities given are stacked
	 * @return True if all entities are passengers
	 */
	public boolean areStacked(Entity[] entities);
	
	/**
	 * Method for seeing whether the entities given are vehicles
	 * @return
	 */
	public boolean areVehicles(Entity[] entities);
	
	/**
	 * Unstacks all entities passed
	 * @return Whether they were unstacked successfully
	 */
	public boolean unstackAll(Entity[] entities);
	
	/**
	 * Stacks all entities passed
	 * @return Whether they were stacked successfully
	 */
	public boolean stackAll();
}
