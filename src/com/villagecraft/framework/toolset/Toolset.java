package com.villagecraft.framework.toolset;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerEvent;

public final class Toolset {

	public String buildPluginTag(String text, ChatColor color, boolean boldtext, boolean boldbrackets) {
		String builder = text;

		if ((boldtext) && (boldbrackets)) {
			builder = "" + color + ChatColor.BOLD + "[" + text + "]" + ChatColor.RESET;
		} else if ((boldtext) && (!boldbrackets)) {
			builder = "" + color + "[" + ChatColor.BOLD + text + ChatColor.RESET + color + "]" + ChatColor.RESET;
		} else if ((!boldtext) && (boldbrackets)) {
			builder = "" + color + ChatColor.BOLD + "[" + ChatColor.RESET + color + text + ChatColor.BOLD + "]"
					+ ChatColor.RESET;
		} else {
			builder = color + "[" + text + "]" + ChatColor.RESET;
		}

		return builder;
	}

	public <K extends PlayerEvent> boolean playerEventsAreCloseby(float distance, K firstEvent, K secondEvent) {
		Player firstPlayer = firstEvent.getPlayer();
		Player secondPlayer = secondEvent.getPlayer();

		if ((Objects.nonNull(firstPlayer)) && (Objects.nonNull(secondPlayer))) {
			Location firstLoc = firstPlayer.getLocation();
			Location secondLoc = secondPlayer.getLocation();

			double measuredDistance = firstLoc.distance(secondLoc);
			if (measuredDistance < distance) {
				return true;
			}
		}
		return false;
	}

	@Deprecated
	public boolean checkNotNulls(Object[] varargs) {
		for (Object obj : varargs) {
			if (obj == null) {
				return false;
			}
		}

		return false;
	}

	public Object getPrivateField(String fieldName, Class<?> clazz, Object object) {
		Object o = null;
		try {
			Field field = clazz.getDeclaredField(fieldName);

			field.setAccessible(true);

			o = field.get(object);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return o;
	}

	public Object invokePrivateMethod(String methodName, Object object, Object[] params) {
		Object result = null;
		@SuppressWarnings("rawtypes")
		Class[] paramClasses = new Class[params.length];

		int adder = 0;
		for (Object temp : params) {
			paramClasses[adder] = temp.getClass();
			adder++;
		}
		try {
			Method method = object.getClass().getDeclaredMethod(methodName, paramClasses);
			method.setAccessible(true);
			result = method.invoke(object, params);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}

		return result;
	}
//
//	public void clearEntityPathfinderGoals(LivingEntity entity) {
//		EntityLiving entityLiving = ((CraftLivingEntity) entity).getHandle();
//		EntityInsentient insentient = (EntityInsentient) entityLiving;
//
//		LinkedHashSet goalB = (LinkedHashSet) Framework.TOOLSET.getPrivateField("b", PathfinderGoalSelector.class,
//				insentient.goalSelector);
//		goalB.clear();
//		LinkedHashSet goalC = (LinkedHashSet) Framework.TOOLSET.getPrivateField("c", PathfinderGoalSelector.class,
//				insentient.goalSelector);
//		goalC.clear();
//		LinkedHashSet targetB = (LinkedHashSet) Framework.TOOLSET.getPrivateField("b", PathfinderGoalSelector.class,
//				insentient.targetSelector);
//		targetB.clear();
//		LinkedHashSet targetC = (LinkedHashSet) Framework.TOOLSET.getPrivateField("c", PathfinderGoalSelector.class,
//				insentient.targetSelector);
//		targetC.clear();
//	}
//
//	public LivingEntity spawnCustomEntity(Class<? extends EntityLiving> clazz, Location location) {
//		Constructor constructor = null;
//		EntityLiving instance = null;
//		try {
//			constructor = clazz.getDeclaredConstructor(new Class[] { net.minecraft.server.v1_12_R1.World.class });
//
//			instance = (EntityLiving) constructor
//					.newInstance(new Object[] { Framework.TOOLSET.convertWorldToNMS(location.getWorld()) });
//			instance.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(),
//					location.getPitch());
//
//			((CraftWorld) location.getWorld()).getHandle().addEntity(instance, SpawnReason.CUSTOM);
//		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
//				| IllegalArgumentException | InvocationTargetException e) {
//			e.printStackTrace();
//		}
//
//		return (LivingEntity) instance.getBukkitEntity();
//	}
//
//	public void registerCustomEntity(Class<? extends CustomEntityTag> clazz) {
//		Map c = (Map) Framework.TOOLSET.getPrivateField("c", EntityTypes.class, null);
//
//		Map d = (Map) Framework.TOOLSET.getPrivateField("d", EntityTypes.class, null);
//
//		Map e = (Map) Framework.TOOLSET.getPrivateField("e", EntityTypes.class, null);
//
//		Map f = (Map) Framework.TOOLSET.getPrivateField("f", EntityTypes.class, null);
//
//		Map g = (Map) Framework.TOOLSET.getPrivateField("g", EntityTypes.class, null);
//
//		String name = clazz.getSimpleName();
//		name = name.substring(1, name.length() - 7);
//		int id = 0;
//		try {
//			id = clazz.getField("ID").getInt(null);
//		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e1) {
//			e1.printStackTrace();
//		}
//
//		c.put(name, clazz);
//		d.put(clazz, name);
//
//		f.put(clazz, Integer.valueOf(id));
//	}
//
//	public net.minecraft.server.v1_12_R1.World convertWorldToNMS(org.bukkit.World world) {
//		return ((CraftWorld) world).getHandle();
//	}
//
//	public void applyBehaviorToEntity(EntityInsentient entity, IndividualBehavior behavior) {
//
//	}

}

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.Toolset JD-Core Version: 0.6.2
 */