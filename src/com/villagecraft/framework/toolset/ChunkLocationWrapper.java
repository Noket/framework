 package com.villagecraft.framework.toolset;


 import java.util.Objects;

import org.bukkit.ChunkSnapshot;

import com.villagecraft.framework.exceptions.NullWrapperException;


 public class ChunkLocationWrapper
 {
	 ChunkSnapshot chunk;
	 Integer xcoord;
	 Integer zcoord;

	
	 public ChunkLocationWrapper(ChunkSnapshot ch)
	 {
		 if (Objects.nonNull(ch)) {
			 this.chunk = ch;
			 this.xcoord = Integer.valueOf(this.chunk.getX());
			 this.zcoord = Integer.valueOf(this.chunk.getZ());
			 } else {
			 this.xcoord = null;
			 this.zcoord = null;
			 }
		 }

	
	 public int getXCoord() throws NullWrapperException {
		 if (Objects.isNull(this.xcoord)) {
			 throw new NullWrapperException();
			 }
		 return this.xcoord.intValue();
		 }

	
	 public int getZCoord() throws NullWrapperException
	 {
		 if (Objects.isNull(this.zcoord)) {
			 throw new NullWrapperException();
			 }
		 return this.zcoord.intValue();
		 }

	
	 public boolean equals(ChunkLocationWrapper wrapper)
	 {
		 int wrapperZ = 0;
		 int wrapperX = 0;

		 try
		 {
			 wrapperX = wrapper.getXCoord();
			 wrapperZ = wrapper.getZCoord();
			 }
		 catch (NullWrapperException e)
		 {
			 return false;
			 }

		 if ((wrapperX == this.xcoord.intValue()) && (wrapperZ == this.zcoord.intValue())) {
			 return true;
			 }
		 return false;
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.ChunkLocationWrapper JD-Core Version:
 * 0.6.2
 */