package com.villagecraft.framework.toolset.game;

public enum Gametypes {
	CAPTURE_THE_FLAG,
	INFECTION,
	FREE_FOR_ALL,
	TEAM,
	KING_OF_THE_HILL,
	PARKOUR,
	SHOOTER,
	BOMB,
	OBJECTIVE,
	DESTINATION,
	MISSION,
	TASK_FORCE;
}
