package com.villagecraft.framework.toolset.game;
/**
 * A game should be able to start
 * @author james
 *
 */
public interface Startable {
	public void start();
}
