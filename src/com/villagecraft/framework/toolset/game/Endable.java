package com.villagecraft.framework.toolset.game;
/**
 * A game should be able to end
 * @author james
 *
 */
public interface Endable {
	public void end();
}
