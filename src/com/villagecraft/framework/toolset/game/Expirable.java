package com.villagecraft.framework.toolset.game;
/**
 * A game should expire, and by extension have a time limit
 * @author james
 *
 */
public interface Expirable {
	public void setTimeLimit(long limit);
	public long getTimeLimit();
	public void setDoesExpire(boolean expires);
	public boolean getDoesExpire();
}
