package com.villagecraft.framework.toolset.game.team;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * A game should be able to have teams
 * @author james
 *
 */
public interface Teamable {
	public void createTeam(byte teamNumber, ChatColor teamColor);
	public byte getNumberOfTeams();
	public void setNumberOfTeams(byte num);
	public Player[] getPlayers(byte teamNumber);
	public short getPlayersOnTeam(ChatColor teamColor);
	public void setPlayersOnTeam(ChatColor teamColor, short numberOfPlayers);
}
