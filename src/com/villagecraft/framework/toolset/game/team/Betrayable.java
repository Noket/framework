package com.villagecraft.framework.toolset.game.team;

import org.bukkit.ChatColor;

public interface Betrayable {
	public boolean getTeamAllowsBetrayals(ChatColor color);
	public void setTeamAllowsBetrayals(ChatColor color, boolean state);
	public boolean getTeamAllowsDeepBetrayals(ChatColor color);
	public void setTeamAllowsDeepBetrayals(ChatColor color, boolean state);
}
