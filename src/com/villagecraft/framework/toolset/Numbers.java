 package com.villagecraft.framework.toolset;


 import java.util.Random;


 public class Numbers
 {
	 public static int generateRandom(int first, int second)
	 {
		 if (first < second)
		 {
			 int randomNum = (int) (second * Math.random() + first);
			 return randomNum;
			 }
		
		 if (second < first)
		 {
			 int randomNum = (int) (first * Math.random() + second);
			 return randomNum;
			 }
		
		 return first;
		 }

	
	 public static double randomDouble(double max, double min)
	 {
		 Random r = new Random();
		 double randomValue = min + (max - min) * r.nextDouble();
		 return randomValue;
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.Number JD-Core Version: 0.6.2
 */