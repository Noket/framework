 package com.villagecraft.framework.toolset;


 import java.io.File;
import java.util.Scanner;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;


 public class JsonUtils
 {
	 public static String getUpdatedStatistics(UUID uuid)
	 {
		 try
		 {
			 World mainworld = (World) Bukkit.getWorlds().get(0);
			
			 File statsfile = new File(mainworld.getWorldFolder(), "stats/" + uuid.toString() + ".json");
			 return new Scanner(statsfile).useDelimiter("\\Z").next();
			 }
		 catch (Exception e)
		 {
			 e.printStackTrace();
			 }
		
		 return null;
		 }

	
	 public static String getValueFor(String jsonString, String value)
	 {
		 try
		 {
			 Object obj = JSONValue.parse(jsonString);
			 JSONObject json = (JSONObject) obj;
			
			 return (String) json.get(value);
			 } catch (Exception e) {
			 e.printStackTrace();
			 }
		return "youbrokemesemicolonopenbracket";
		 }

	
	 public static boolean hasValueFor(String jsonString, String value)
	 {
		 try
		 {
			 Object obj = JSONValue.parse(jsonString);
			 JSONObject json = (JSONObject) obj;
			
			 return json.containsKey(value);
			 } catch (Exception e) {
			 e.printStackTrace();
			 }
		 return false;
		 }
	 }

/*
 * Location: Z:\media\james\Storage\ProgrammingTools\Java
 * Decompiler\Targets\Framework.jar Qualified Name:
 * com.villagecraft.framework.toolset.JsonUtils JD-Core Version: 0.6.2
 */