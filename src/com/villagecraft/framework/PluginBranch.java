package com.villagecraft.framework;

import java.io.File;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPluginLoader;

import com.villagecraft.framework.exceptions.FailedDisableException;
import com.villagecraft.framework.exceptions.FailedEnableException;
import com.villagecraft.framework.exceptions.FailedLoadException;

/**
 * Standardizes methods to allow seamless communication between branches. For the branch to be created, 
 * register the plugin branch by using "PluginDelegate.registerBranch(Branch.class)". This interface already extends listener, so branches
 * don't need to implement it.
 * @author james
 *
 */
public interface PluginBranch extends Listener {
	
	/**
	 * This method returns the instance of the class that was created by the plugin manager.
	 * @return 
	 * @return The instance of the implementing class
	 */
	//public <K extends PluginDelegatable> Class<? extends PluginDelegatable> returnInstance();
	
	
	/**
	 * In the future, this will use reflection to override Bukkit's protection system, so you can register commands dynamically.
	 * <br><br>As of yet, this is not implemented.
	 * @param loader
	 * @param description
	 * @param dataFolder
	 * @param file
	 */
	@Deprecated
	public void registerCommand(final JavaPluginLoader loader, final PluginDescriptionFile description, final File dataFolder, final File file);
	
	/**
	 * Runs code that should execute when the branch is enabled. Any code that would go in "onLoad()"
	 * should be placed in this method. This method must be passed the VcMod object!
	 */
	public void load() throws FailedLoadException;
	
	/**
	 * Runs code that should execute when the branch is enabled. Any code that would go in "onEnable()"
	 * should be placed in this method. This method must be passed the VcMod object!
	 */
	public void enable() throws FailedEnableException;
	
	/**
	 * Runs code that should execute when the branch is disabled. Any code that would go in "onDisable()"
	 * should be placed in this method.
	 */
	public void disable() throws FailedDisableException;
	
	/**
	 * Branches should be able to accept and process commands sent from the main plugin. Any command
	 * handling should be done via this method
	 * @param player - The command sender, not necessarily a player
	 * @param cmd - The Command object. Used to obtain the command name.
	 * @param label - This is possible alises the command can take, defined in the config.
	 * @param args - The arguments passed to the command
	 */
	public abstract void command(CommandSender player, Command cmd, String label, String[] args);

	
}
