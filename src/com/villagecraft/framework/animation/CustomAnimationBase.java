package com.villagecraft.framework.animation;

import com.villagecraft.framework.events.custom.CustomEventObservable;

public abstract class CustomAnimationBase extends CustomEventObservable implements EventAnimatable{
	
	public CustomAnimationBase() {
		animate();
	}
}
