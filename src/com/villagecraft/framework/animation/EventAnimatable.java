package com.villagecraft.framework.animation;

public interface EventAnimatable {

	public void animate();

}
