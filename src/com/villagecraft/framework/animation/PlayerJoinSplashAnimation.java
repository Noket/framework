package com.villagecraft.framework.animation;

import java.util.Objects;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;

import com.villagecraft.framework.Framework;
import com.villagecraft.framework.toolset.Hooks;

public class PlayerJoinSplashAnimation extends CustomAnimationBase{

	PlayerJoinEvent event;
	
	public PlayerJoinSplashAnimation(PlayerJoinEvent event){
		super();
		this.event = event;
	}
	
	@Override
	public void animate() {
		final ConsoleCommandSender sender = Hooks.hBukkit.commandsender();
		
		final Player player = event.getPlayer();
		
		Bukkit.getScheduler().runTaskLaterAsynchronously(Framework.getPlugin(Framework.class), new Runnable() {
			public void run() {
				if (Objects.nonNull(player))
				{
					if (player.isOnline())
					{
						Bukkit.dispatchCommand(sender, "title " + player.getName() + " title [\"\",{\"text\":\" - \",\"color\":\"yellow\",\"bold\":\"true\",\"italic\":\"true\"},{\"text\":\"V\",\"color\":\"gold\",\"bold\":\"true\",\"italic\":\"true\"},{\"text\":\"ILLAGE\",\"color\":\"yellow\",\"bold\":\"true\",\"italic\":\"true\"},{\"text\":\"C\",\"color\":\"gold\",\"bold\":\"true\",\"italic\":\"true\"},{\"text\":\"RAFT -\",\"color\":\"yellow\",\"bold\":\"true\",\"italic\":\"true\"}]");
						Bukkit.dispatchCommand(sender, "title " + player.getName()	+ " subtitle [\"\",{\"text\":\"Since 2011\",\"color\":\"dark_green\",\"italic\":\"true\"}]");
					}
				}
			}
		}, 20L);
		
		Bukkit.getScheduler().runTaskLaterAsynchronously(Framework.getPlugin(Framework.class), new Runnable() {
			public void run() {
				if(Objects.nonNull(player))
				{
					if(player.isOnline())
					{
						if (player.getLastPlayed() < 10000L)
						{
							Bukkit.dispatchCommand(sender,"title " + player.getName() + " title [\"\",{\"text\":\"Welcome, "+ player.getName() + "\",\"color\":\"gray\"}]");
							Bukkit.dispatchCommand(sender,"title " + player.getName() + " subtitle [\"\",{\"text\":\"Running Minecraft 1.9\",\"color\":\"gold\",\"italic\":\"true\"}]");
						}
						else
						{
							Bukkit.dispatchCommand(sender,"title " + player.getName() + " title [\"\",{\"text\":\" \",\"color\":\"white\"}]");
							Bukkit.dispatchCommand(sender,"title " + player.getName() + " subtitle [\"\",{\"text\":\"Running Minecraft 1.9\",\"color\":\"gold\",\"italic\":\"true\"}]");
						}
					}
				}
			}
		}, 130L);
	}

}
